FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive 

RUN apt-get update && \ 
    apt-get install -y  make g++ git cmake qt5-default 

RUN git clone https://gitlab.com/mowgliluciano/azrs-tracking

WORKDIR /azrs-tracking

RUN ./run.sh

CMD cd bin && ./Logic_Circuit_Simulator
    
