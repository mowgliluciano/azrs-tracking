#include <circuit.hpp>

#ifdef __USE_UTIL__
#include <util.hpp>
#endif

void connectHelper(SingleInput &si, std::weak_ptr<Signal> sig, Circuit *c) {
    auto curr_sig = si.getSignal();
    const auto curr_sig_val = si.value();
    if (!curr_sig.expired())
	QObject::disconnect(curr_sig.lock().get(),
			    &Signal::changedValue,
			    c,
			    &Circuit::re_evaluate);
    si.putSignal(sig);
    const auto next_sig_val = si.value();
    if (!sig.expired())
	QObject::connect(
	    sig.lock().get(), &Signal::changedValue, c, &Circuit::re_evaluate);
    if (next_sig_val != curr_sig_val)
	c->re_evaluate();
}

void JustInputCircuit::connectInput(std::weak_ptr<Signal> sig, unsigned u) {
#ifdef __USE_UTIL__
    if (u)
	util::upozorenje(
	    std::string(
		"SingleInputCircuit.connectInput: has single input slot, but ")
		.append(std::to_string(u))
		.append(" was passed."));
#endif
    connectHelper(m_sig, sig, this);
}

auto JustInputCircuit::getSignal(unsigned) const -> std::weak_ptr<Signal> {
#ifdef __USE_UTIL__
    util::greska("JustInputCircuit.getSignal: not allowed action");
#endif
    return std::make_shared<Signal>();
}

void JustOutputCircuit::connectInput(std::weak_ptr<Signal>, unsigned) {
#ifdef __USE_UTIL__
    util::greska("JustOutputCircuit.connectInput: not allowed action");
#endif
}

auto JustOutputCircuit::getSignal(unsigned u) const -> std::weak_ptr<Signal> {
#ifdef __USE_UTIL__
    if (u)
	util::greska(std::string("SingleOutputCircuit.getSignal: has only one "
				 "output slot, but in function 'getSignal' ")
			 .append(std::to_string(u))
			 .append(" was passed"));
#endif
    return m_sig.getSignal();
}

void SingleInputOutputCircuit::connectInput(std::weak_ptr<Signal> sig,
					    unsigned u) {
#ifdef __USE_UTIL__
    if (u)
	util::upozorenje(
	    std::string("SingleInputOutputCircuit.connectInput: has "
			"single input slot, but ")
		.append(std::to_string(u))
		.append(" was passed."));
#endif

    connectHelper(m_sig_in, sig, this);
}

auto SingleInputOutputCircuit::getSignal(unsigned u) const -> std::weak_ptr<Signal> {
#ifdef __USE_UTIL__
    if (u)
	util::greska(
	    std::string("SingleInputOutputCircuit.getSignal: has only one "
			"output slot, but in function 'getSignal' ")
		.append(std::to_string(u))
		.append(" was passed"));
#endif
    return m_sig_out.getSignal();
}

void MultiInputCircuit::connectInput(std::weak_ptr<Signal> sig, unsigned u) {
#ifdef __USE_UTIL__
    if (u >= getInputN())
	util::upozorenje(
	    std::string("MultiInputCircuit.connectInput: instance has  ")
		.append(std::to_string(getInputN()))
		.append(" input slots, but ")
		.append(std::to_string(u))
		.append(" was passed"));
    else
#endif
	connectHelper(m_sig_in[u], sig, this);
}

auto MultiInputCircuit::getSignal(unsigned u) const -> std::weak_ptr<Signal> {
#ifdef __USE_UTIL__
    if (u)
	util::greska(
	    std::string("MultiInputCircuit.getSignal: has only one output "
			"slot, but in function 'getSignal' ")
		.append(std::to_string(u))
		.append(" was passed"));
#endif
    return m_sig_out.getSignal();
}

void MultiOutputCircuit::connectInput(std::weak_ptr<Signal> sig, unsigned u) {
#ifdef __USE_UTIL__
    if (u)
	util::upozorenje(
	    std::string(
		"MultiOutputCircuit.connectInput: has single input slot, but ")
		.append(std::to_string(u))
		.append(" was passed."));
    else
#endif
	connectHelper(m_sig_in, sig, this);
}

auto MultiOutputCircuit::getSignal(unsigned u) const -> std::weak_ptr<Signal> {
#ifdef __USE_UTIL__
    if (u >= getOutputN())
	util::greska(
	    std::string("MultiOutputCircuit.connectInput: instance has  ")
		.append(std::to_string(getOutputN()))
		.append(" output slots, but ")
		.append(std::to_string(u))
		.append(" was passed"));
#endif
    return m_sig_out.getSignal(u);
}

void MultiInputOutputCircuit::connectInput(std::weak_ptr<Signal> sig,
					   unsigned u) {
#ifdef __USE_UTIL__
    if (u >= getInputN())
	util::upozorenje(
	    std::string("MultiInputOutputCircuit.connectInput: instance has  ")
		.append(std::to_string(getInputN()))
		.append(" input slots, but ")
		.append(std::to_string(u))
		.append(" was passed"));
    else
#endif
	connectHelper(m_sig_in[u], sig, this);
}

auto MultiInputOutputCircuit::getSignal(unsigned u) const -> std::weak_ptr<Signal> {
#ifdef __USE_UTIL__
    if (u >= getOutputN())
	util::greska(
	    std::string("MultiInputOutputCircuit.connectInput: instance has  ")
		.append(std::to_string(getOutputN()))
		.append(" output slots, but ")
		.append(std::to_string(u))
		.append(" was passed"));
#endif
    return m_sig_out.getSignal(u);
}
