#include <QCursor>
#include <QGraphicsSceneMouseEvent>
#include <circuit.hpp>
#include <cmath>
// Specific circuit function implementations

Circuit::Circuit(CircuitType c, QGraphicsItem *parent)
    : m_ctype(c)
    , QGraphicsRectItem(parent)
    , m_circuitRotation(RotationType::Right)
    , m_pen{Qt::black, 2, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin}
    , m_inputPostions{}
    , m_outputPostions{} {
    setFlag(QGraphicsRectItem::ItemIsMovable, true);
    setFlag(QGraphicsRectItem::ItemIsSelectable, true);
    //        setFlag(QGraphicsItem::ItemSendsGeometryChanges,true);
}

void Circuit::re_evaluate() {
    evaluate();
    update();
}

auto rotatePoint(QPointF point, RotationType rotation) -> QPointF {
    switch (rotation) {
    case Down: return {-point.ry(), point.rx()};
    case Left: return {-point.rx(), -point.ry()};
    case Up: return {point.ry(), -point.rx()};
    default: return point;
    }
}

auto Circuit::getInputPositions() const -> const std::vector<QPointF> {
    if (m_circuitRotation == RotationType::Right)
	return m_inputPostions;
    else {
	std::vector<QPointF> temp;
	for (auto point : m_inputPostions)
	    temp.push_back(rotatePoint(point, m_circuitRotation));
	return temp;
    }
}

auto Circuit::getOutPutPositions() const -> const std::vector<QPointF> {
    if (m_circuitRotation == RotationType::Right)
	return m_outputPostions;
    else {
	std::vector<QPointF> temp;
	for (auto point : m_outputPostions)
	    temp.push_back(rotatePoint(point, m_circuitRotation));
	return temp;
    }
}

CircuitId::CircuitId() : SingleInputOutputCircuit(CircuitType::Id) {
    m_inputPostions.emplace_back(-19, 0);
    m_outputPostions.emplace_back(22, 0);
}

void CircuitId::evaluate() { m_sig_out.put(m_sig_in.value()); }

void CircuitAnd::evaluate() {
    SignalType sig = m_sig_in.prioritySignalSearcher(
	SignalType::Not_Connected, SignalType::False, SignalType::True);
    m_sig_out.put(sig);
}

CircuitOr::CircuitOr(unsigned i) : MultiInputCircuit(CircuitType::Or, i) {}

void CircuitOr::evaluate() {
    SignalType sig = m_sig_in.prioritySignalSearcher(
	SignalType::Not_Connected, SignalType::True, SignalType::False);
    m_sig_out.put(sig);
}

CircuitXor::CircuitXor(unsigned i) : MultiInputCircuit(CircuitType::Xor, i) {}

void CircuitXor::evaluate() {
    if (m_sig_in.hasSingalType(SignalType::Not_Connected))
	m_sig_out.put(SignalType::Not_Connected);
    // svi isti->0
    else {
	if (m_sig_in.hasAllEqualSignalType())
	    m_sig_out.put(SignalType::False);
	else
	    m_sig_out.put(SignalType::True);
    }
}

// not circuit
CircuitNon::CircuitNon() : SingleInputOutputCircuit(CircuitType::Non) {
    m_inputPostions.emplace_back(-19, 0);
    m_outputPostions.emplace_back(22, 0);
}

void CircuitNon::evaluate() {
    m_sig_out.put(Signal(m_sig_in.value()).negateValue());
}

CircuitNand::CircuitNand(unsigned num_of_inputs)
    : MultiInputCircuit(CircuitType::Nand, num_of_inputs) {}

void CircuitNand::evaluate() {
    SignalType sig = m_sig_in.prioritySignalSearcher(
	SignalType::Not_Connected, SignalType::False, SignalType::True);
    sig = Signal(sig).negateValue();
    m_sig_out.put(sig);
}

CircuitNor::CircuitNor(unsigned num_of_inputs)
    : MultiInputCircuit(CircuitType::Nor, num_of_inputs) {}

void CircuitNor::evaluate() {
    SignalType sig = m_sig_in.prioritySignalSearcher(
	SignalType::Not_Connected, SignalType::True, SignalType::False);
    sig = Signal(sig).negateValue();
    m_sig_out.put(sig);
}

CircuitNxor::CircuitNxor(unsigned i)
    : MultiInputCircuit(CircuitType::Nxor, i) {}

void CircuitNxor::evaluate() {
    if (m_sig_in.hasSingalType(SignalType::Not_Connected))
	m_sig_out.put(SignalType::Not_Connected);
    // svi isti->1
    else {
	if (m_sig_in.hasAllEqualSignalType())
	    m_sig_out.put(SignalType::True);
	else
	    m_sig_out.put(SignalType::False);
    }
}

void Multiplexer::SetInputPositions() {
    int height = (m_inputSlots + 1) * 20;
    int width = (m_controlSlots + 1) * 24;

    unsigned i;
    int x, y;
    for (i = 1; i <= m_inputSlots; i++) {
	y = height / (m_inputSlots + 1) * i + 10;
	m_inputPostions.emplace_back(QPoint(0, y));
    }

    std::reverse(m_inputPostions.begin(), m_inputPostions.end());

    for (i = 1; i <= m_controlSlots; i++) {
	x = width / (m_controlSlots + 1) * i + 10;
	m_inputPostions.emplace_back(QPoint(x, height + 20));
    }
}

void Multiplexer::SetOutputPositions() {
    int height = (m_inputSlots + 1) * 20;
    int width = (m_controlSlots + 1) * 24;
    m_outputPostions.emplace_back(QPoint(width + 20, height / 2 + 10));
}

Multiplexer::Multiplexer(unsigned inputSlots, unsigned controlSlots)
    : MultiInputCircuit(CircuitType::Multiplex, inputSlots + controlSlots)
    , m_inputSlots(inputSlots)
    , m_controlSlots(controlSlots) {
    SetInputPositions();
    SetOutputPositions();
}

void Multiplexer::evaluate() {

    if (m_sig_in.hasSingalType(SignalType::Not_Connected, m_inputSlots)) {
	m_sig_out.put(SignalType::Not_Connected);
	return;
    }

    unsigned slotPosition = 0;
    unsigned j = 0;
    for (unsigned i = m_inputSlots; i < m_controlSlots + m_inputSlots; i++) {
	if (m_sig_in.checkInputSignalValue(i) == SignalType::True) {
	    slotPosition = slotPosition + std::pow(2, j);
	}
	j++;
    }

    auto s = m_sig_in.checkInputSignalValue(slotPosition);
    m_sig_out.put(s);
}

void Demultiplexer::SetInputPositions() {
    const auto numOfOutputs = getOutputN();
    int height = (numOfOutputs + 1) * 20;
    int width = (m_controlSlots + 1) * 24;

    m_inputPostions.emplace_back(QPoint(0, height / 2 + 10));

    unsigned i;
    int x;
    for (i = 1; i <= m_controlSlots; i++) {
	x = width / (m_controlSlots + 1) * i + 10;
	m_inputPostions.emplace_back(QPoint(x, height + 20));
    }
}

void Demultiplexer::SetOutputPositions() {

    const auto numOfOutputs = getOutputN();
    int height = (numOfOutputs + 1) * 20;
    int width = (m_controlSlots + 1) * 24;

    unsigned i;
    int y;
    for (i = 1; i <= numOfOutputs; i++) {
	y = height / (numOfOutputs + 1) * i + 10;
	m_outputPostions.emplace_back(QPoint(width + 20, y));
    }
    std::reverse(m_outputPostions.begin(), m_outputPostions.end());
}

Demultiplexer::Demultiplexer(unsigned inputSlots, unsigned controlSlots)
    : MultiInputOutputCircuit(CircuitType::Demultiplex,
			      inputSlots + controlSlots,
			      std::pow(2, controlSlots))
    , m_inputSlots(inputSlots)
    , m_controlSlots(controlSlots) {
    SetInputPositions();
    SetOutputPositions();
}

void Demultiplexer::evaluate() {
    if (m_sig_in.hasSingalType(SignalType::Not_Connected)) {
	m_sig_out.putAll(SignalType::Not_Connected);
	return;
    }

    // Reseting outputs to false since only 1 can be true
    // so they can't keep values from previous iterations
    m_sig_out.putAll(SignalType::False);

    unsigned slotPosition = 0;
    unsigned j = 0;
    for (unsigned i = m_inputSlots; i < m_controlSlots + m_inputSlots; i++) {
	if (m_sig_in.checkInputSignalValue(i) == SignalType::True) {
	    slotPosition += std::pow(2, j);
	}
	j++;
    }

    auto s = m_sig_in.checkInputSignalValue(0);
    m_sig_out.put(s, slotPosition);
}

void CircuitEncoder::SetInputPositions() {
    const auto numOfInputs = getInputN();

    int height = (numOfInputs + 1) * 20;

    unsigned i;
    int y;
    for (i = 1; i <= numOfInputs; i++) {
	y = height / (numOfInputs + 1) * i + 10;
	m_inputPostions.emplace_back(0, y);
    }
    std::reverse(m_inputPostions.begin(), m_inputPostions.end());
}

void CircuitEncoder::SetOutputPositions() {
    const auto numOfInputs = getInputN();
    const auto numOfOutputs = getOutputN();

    int height = (numOfInputs + 1) * 20;
    int width = 80;

    unsigned i;
    int y;
    for (i = 1; i <= numOfOutputs; i++) {
	y = height / (numOfOutputs + 1) * i + 10;
	m_outputPostions.emplace_back(width + 20, y);
    }
}

CircuitEncoder::CircuitEncoder(unsigned num_of_inputs, unsigned num_of_outputs)
    : MultiInputOutputCircuit(
	  CircuitType::Encoder, num_of_inputs, num_of_outputs) {
    SetInputPositions();
    SetOutputPositions();
}

void CircuitEncoder::evaluate() {
    if (m_sig_in.hasSingalType(SignalType::Not_Connected)) {
	m_sig_out.putAll(SignalType::Not_Connected);
	return;
    }

    m_sig_out.putAll(SignalType::False);

    auto numOfInputs = getInputN();
    auto numOfOutputs = getOutputN();
    int sum = 0;
    for (unsigned int i = 0; i < numOfInputs; i++) {
	if (m_sig_in.checkInputSignalValue(i) == SignalType::True) {
	    sum += i;
	    break;
	}
    }

    for (unsigned int i = 0; i < numOfOutputs; ++i) {
	if (sum % 2 == 1) {
	    m_sig_out.put(SignalType::True, numOfOutputs - i - 1);
	}
	sum = sum / 2;
    }
}

void CircuitDecoder::SetInputPositions() {
    const auto numOfInputs = getInputN();
    const auto numOfOutputs = getOutputN();

    int height = (numOfOutputs + 1) * 20;
    // int width = 80;

    unsigned i;
    int y;
    for (i = 1; i <= numOfInputs; i++) {
	y = height / (numOfInputs + 1) * i + 10;
	m_inputPostions.emplace_back(0, y);
    }
    std::reverse(m_inputPostions.begin(), m_inputPostions.end());
}

void CircuitDecoder::SetOutputPositions() {
    const auto numOfOutputs = getOutputN();

    int height = (numOfOutputs + 1) * 20;
    int width = 80;

    unsigned i;
    int y;
    for (i = 1; i <= numOfOutputs; i++) {
	y = height / (numOfOutputs + 1) * i + 10;
	m_outputPostions.emplace_back(width + 20, y);
    }

    std::reverse(m_outputPostions.begin(), m_outputPostions.end());
}

CircuitDecoder::CircuitDecoder(unsigned num_of_inputs, unsigned num_of_outputs)
    : MultiInputOutputCircuit(
	  CircuitType::Decoder, num_of_inputs, num_of_outputs) {
    SetInputPositions();
    SetOutputPositions();
}

void CircuitDecoder::evaluate() {
    if (m_sig_in.hasSingalType(SignalType::Not_Connected)) {
	m_sig_out.putAll(SignalType::Not_Connected);
	return;
    }
    m_sig_out.putAll(SignalType::False);

    unsigned slotPosition = 0;
    for (unsigned i = 0; i < getInputN(); i++) {
	if (m_sig_in.checkInputSignalValue(i) == SignalType::True) {
	    slotPosition += std::pow(2, i);
	}
    }
    m_sig_out.put(SignalType::True, slotPosition);
}

CircuitHalfAdder::CircuitHalfAdder()
    : MultiInputOutputCircuit(CircuitType::HalfAdder, 2, 2) {
    m_inputPostions.emplace_back(0, 76);
    m_inputPostions.emplace_back(0, 43);
    m_outputPostions.emplace_back(100, 76);
    m_outputPostions.emplace_back(100, 43);
}

void CircuitHalfAdder::evaluate() {
    if (m_sig_in.hasSingalType(SignalType::Not_Connected)) {
	m_sig_out.putAll(SignalType::Not_Connected);
	return;
    }

    SignalType xor_s = (m_sig_in.hasAllEqualSignalType()) ? SignalType::False
							  : SignalType::True;
    SignalType and_s = m_sig_in.prioritySignalSearcher(
	SignalType::False, SignalType::True, SignalType::Not_Connected);

    m_sig_out.put(xor_s, 0);
    m_sig_out.put(and_s, 1);
}

CircuitAdder::CircuitAdder()
    : MultiInputOutputCircuit(CircuitType::Adder, 3, 2) {
    m_inputPostions.emplace_back(0, 85);
    m_inputPostions.emplace_back(0, 60);
    m_inputPostions.emplace_back(0, 35);
    m_outputPostions.emplace_back(100, 76);
    m_outputPostions.emplace_back(100, 43);
}

void CircuitAdder::evaluate() {
    if (m_sig_in.hasSingalType(SignalType::Not_Connected)) {
	m_sig_out.putAll(SignalType::Not_Connected);
	return;
    }

    if (m_sig_in.hasAllEqualSignalType()) {
	m_sig_out.putAll(m_sig_in.checkInputSignalValue(0));
	return;
    }

    SignalType second = (m_sig_in.hasAllEqualSignalType(1)) ? SignalType::True
							    : SignalType::False;

    if (m_sig_in.checkInputSignalValue(0) == SignalType::False) {
	m_sig_out.put(Signal(second).negateValue(), 0);
	m_sig_out.put(second, 1);
    } else {
	m_sig_out.put(second, 0);
	m_sig_out.put(Signal(second).negateValue(), 1);
    }
}

CircuitHalfSubtractor::CircuitHalfSubtractor()
    : MultiInputOutputCircuit(CircuitType::HalfSubtractor, 2, 2) {
    m_inputPostions.emplace_back(0, 76);
    m_inputPostions.emplace_back(0, 43);
    m_outputPostions.emplace_back(100, 76);
    m_outputPostions.emplace_back(100, 43);
}

void CircuitHalfSubtractor::evaluate() {

    if (m_sig_in.hasSingalType(SignalType::Not_Connected)) {
	m_sig_out.putAll(SignalType::Not_Connected);
	return;
    }

    SignalType xor_s = (m_sig_in.hasAllEqualSignalType()) ? SignalType::False
							  : SignalType::True;
    m_sig_out.put(xor_s, 0);

    if (m_sig_in.checkInputSignalValue(0) == SignalType::True ||
	m_sig_in.checkInputSignalValue(1) == SignalType::False)
	m_sig_out.put(SignalType::False, 1);
    else
	m_sig_out.put(SignalType::True, 1);
}

CircuitSubtractor::CircuitSubtractor()
    : MultiInputOutputCircuit(CircuitType::Subtractor, 3, 2) {
    m_inputPostions.emplace_back(0, 85);
    m_inputPostions.emplace_back(0, 60);
    m_inputPostions.emplace_back(0, 35);
    m_outputPostions.emplace_back(100, 76);
    m_outputPostions.emplace_back(100, 43);
}

void CircuitSubtractor::evaluate() {
    if (m_sig_in.hasSingalType(SignalType::Not_Connected)) {
	m_sig_out.putAll(SignalType::Not_Connected);
	return;
    }

    if (m_sig_in.hasAllEqualSignalType()) {
	m_sig_out.putAll(m_sig_in.checkInputSignalValue(0));
	return;
    }

    if (m_sig_in.hasAllEqualSignalType(1)) {
	m_sig_out.put(m_sig_in.checkInputSignalValue(0), 0);
	m_sig_out.put(m_sig_in.checkInputSignalValue(1), 1);
	return;
    }

    m_sig_out.putAll(Signal(m_sig_in.checkInputSignalValue(0)).negateValue());
}

auto CircuitClock::getBig() const -> QPointF {
    return QPointF(5 * std::cos((m_tb * M_PI) / 30),
		   5 * std::sin((m_tb * M_PI) / 30));
}

auto CircuitClock::getSmall() const -> QPointF {
    return QPointF(3 * std::cos((m_ts * M_PI) / 6),
		   3 * std::sin((m_ts * M_PI) / 6));
}

CircuitClock::CircuitClock(unsigned t)
    : JustOutputCircuit(CircuitType::Clock)
    , m_tb(45)
    , m_ts(0)
    , m_interval(t)
    , m_paused(false) {
    m_sig.put(SignalType::False);
    m_timer = new QTimer(this);
    QObject::connect(
	m_timer, &QTimer::timeout, this, &CircuitClock::re_evaluate);
    m_timer->start(t);
    m_outputPostions.emplace_back(20, 0);
}

void CircuitClock::evaluate() {
    //    std::cout << "CLK EVALUATE" << std::endl;
    auto val = m_sig.value();
    if (!m_paused) {
	val = Signal(val).negateValue();
	++m_tb %= 60;
	if (m_tb == 45)
	    ++m_ts %= 12;
    }
    m_sig.put(val);
}

void CircuitClock::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *) {
    m_paused = !m_paused;
    if (m_paused)
	m_timer->stop();
    else
	m_timer->start(m_interval);
}

CircuitSpliter::CircuitSpliter(unsigned num_of_outputs)
    : MultiOutputCircuit(CircuitType::Spliter, num_of_outputs) {
    m_inputPostions.emplace_back(QPoint(-15, 0));
    m_outputPostions.emplace_back(QPoint(15, 0));
    m_outputPostions.emplace_back(QPoint(0, 15));
    m_outputPostions.emplace_back(QPoint(0, -15));
}

void CircuitSpliter::evaluate() { m_sig_out.putAll(m_sig_in.value()); }

auto CircuitSpliter::getAllSignals() const -> std::vector<SignalType> {
    std::vector<SignalType> xs;
    for (auto i = 0u; i < m_sig_out.getOutputN(); i++)
	xs.push_back(m_sig_out.getSignal(i).lock()->value());

    return xs;
}

JKFlipFlop::JKFlipFlop(unsigned num_of_inputs, unsigned num_of_outputs)
    : MultiInputOutputCircuit(
	  CircuitType::JKFlipFlop, num_of_inputs, num_of_outputs) {
    m_inputPostions.emplace_back(0, 90);
    m_inputPostions.emplace_back(0, 60);
    m_inputPostions.emplace_back(0, 30);
    m_outputPostions.emplace_back(100, 80);
    m_outputPostions.emplace_back(100, 40);
}

void JKFlipFlop::evaluate() {
    SignalType first_input = m_sig_in.checkInputSignalValue(0);
    SignalType second_input = m_sig_in.checkInputSignalValue(2);
    SignalType clock_input = m_sig_in.checkInputSignalValue(1);

    // doesn't change state when both input signals are false
    if (first_input == SignalType::False && second_input == SignalType::False) {
	return;
    }

    // doesn't change state when clock is eather false or not connected
    if (clock_input == SignalType::False ||
	clock_input == SignalType::Not_Connected) {
	return;
    }

    // on positive tick of the clock
    if (first_input == SignalType::True && second_input == SignalType::False &&
	clock_input == SignalType::True) {
	m_sig_out.put(SignalType::False, 1);
	m_sig_out.put(SignalType::True, 0);
    } else if (first_input == SignalType::False &&
	       second_input == SignalType::True &&
	       clock_input == SignalType::True) {
	m_sig_out.put(SignalType::True, 1);
	m_sig_out.put(SignalType::False, 0);
    } else if (first_input == SignalType::True &&
	       second_input == SignalType::True &&
	       clock_input == SignalType::True) {
	if (m_sig_out.getSignal(0).lock()->value() ==
		SignalType::Not_Connected &&
	    m_sig_out.getSignal(1).lock()->value() ==
		SignalType::Not_Connected) {
	    m_sig_out.put(SignalType::True, 0);
	    m_sig_out.put(SignalType::False, 1);
	} else if (m_sig_out.getSignal(0).lock()->value() == SignalType::True &&
		   m_sig_out.getSignal(1).lock()->value() ==
		       SignalType::False) {
	    m_sig_out.put(SignalType::False, 0);
	    m_sig_out.put(SignalType::True, 1);
	} else if (m_sig_out.getSignal(0).lock()->value() ==
		       SignalType::False &&
		   m_sig_out.getSignal(1).lock()->value() == SignalType::True) {
	    m_sig_out.put(SignalType::True, 0);
	    m_sig_out.put(SignalType::False, 1);
	}
    }
}

DFlipFlop::DFlipFlop(unsigned num_of_inputs, unsigned num_of_outputs)
    : MultiInputOutputCircuit(
	  CircuitType::DFlipFlop, num_of_inputs, num_of_outputs) {
    m_inputPostions.emplace_back(0, 60);
    m_inputPostions.emplace_back(0, 30);
    m_outputPostions.emplace_back(100, 80);
    m_outputPostions.emplace_back(100, 40);
}

void DFlipFlop::evaluate() {
    SignalType first_input = m_sig_in.checkInputSignalValue(1);
    SignalType clock_input = m_sig_in.checkInputSignalValue(0);

    // doesn't change when state is false
    // if(first_input == SignalType::False){
    //   return;
    //}

    // on positive tick of the clock
    if (first_input == SignalType::True && clock_input == SignalType::True) {
	m_sig_out.put(SignalType::False, 1);
	m_sig_out.put(SignalType::True, 0);
    } else if (first_input == SignalType::False &&
	       clock_input == SignalType::True) {
	m_sig_out.put(SignalType::True, 1);
	m_sig_out.put(SignalType::False, 0);
    }
}

TFlipFlop::TFlipFlop(unsigned num_of_inputs, unsigned num_of_outputs)
    : MultiInputOutputCircuit(
	  CircuitType::TFlipFlop, num_of_inputs, num_of_outputs) {
    m_inputPostions.emplace_back(0, 60);
    m_inputPostions.emplace_back(0, 30);
    m_outputPostions.emplace_back(100, 80);
    m_outputPostions.emplace_back(100, 40);
}

void TFlipFlop::evaluate() {
    SignalType first_input = m_sig_in.checkInputSignalValue(1);
    SignalType clock_input = m_sig_in.checkInputSignalValue(0);

    // doesn't change when state is false
    if (first_input == SignalType::False) {
	return;
    }

    // on positive tick of the clock
    if (first_input == SignalType::True && clock_input == SignalType::True) {
	if (m_sig_out.getSignal(0).lock()->value() ==
		SignalType::Not_Connected &&
	    m_sig_out.getSignal(1).lock()->value() ==
		SignalType::Not_Connected) {
	    m_sig_out.put(SignalType::True, 0);
	    m_sig_out.put(SignalType::False, 1);
	} else if (m_sig_out.getSignal(0).lock()->value() == SignalType::True &&
		   m_sig_out.getSignal(1).lock()->value() ==
		       SignalType::False) {
	    m_sig_out.put(SignalType::False, 0);
	    m_sig_out.put(SignalType::True, 1);
	} else if (m_sig_out.getSignal(0).lock()->value() ==
		       SignalType::False &&
		   m_sig_out.getSignal(1).lock()->value() == SignalType::True) {
	    m_sig_out.put(SignalType::True, 0);
	    m_sig_out.put(SignalType::False, 1);
	}
    }
}

SRFlipFlop::SRFlipFlop(unsigned num_of_inputs, unsigned num_of_outputs)
    : MultiInputOutputCircuit(
	  CircuitType::SRFlipFlop, num_of_inputs, num_of_outputs) {
    m_inputPostions.emplace_back(0, 90);
    m_inputPostions.emplace_back(0, 60);
    m_inputPostions.emplace_back(0, 30);
    m_outputPostions.emplace_back(100, 80);
    m_outputPostions.emplace_back(100, 40);
}

void SRFlipFlop::evaluate() {
    SignalType first_input = m_sig_in.checkInputSignalValue(0);
    SignalType second_input = m_sig_in.checkInputSignalValue(2);
    SignalType clock_input = m_sig_in.checkInputSignalValue(1);

    // doesn't change state when both input signals are false
    if (first_input == SignalType::False && second_input == SignalType::False) {
	return;
    }

    // doesn't change state when clock is eather false or not connected
    if (clock_input == SignalType::False ||
	clock_input == SignalType::Not_Connected) {
	return;
    }

    // on positive tick of the clock
    if (first_input == SignalType::True && second_input == SignalType::False &&
	clock_input == SignalType::True) {
	m_sig_out.put(SignalType::False, 1);
	m_sig_out.put(SignalType::True, 0);
    } else if (first_input == SignalType::False &&
	       second_input == SignalType::True &&
	       clock_input == SignalType::True) {
	m_sig_out.put(SignalType::False, 0);
	m_sig_out.put(SignalType::True, 1);
    } else if (first_input == SignalType::True &&
	       second_input == SignalType::True &&
	       clock_input == SignalType::True) {
	// invalid option
	m_sig_out.put(SignalType::Not_Connected, 0);
	m_sig_out.put(SignalType::Not_Connected, 1);
    }
}

// Ima 3 izlaza:
// G (X>Y)
// E (X=Y)
// L (X<Y)
CircuitComparator::CircuitComparator()
    : MultiInputOutputCircuit(CircuitType::Comparator, 2, 3) {
    m_inputPostions.emplace_back(0, 76);
    m_inputPostions.emplace_back(0, 43);
    m_outputPostions.emplace_back(100, 85);
    m_outputPostions.emplace_back(100, 60);
    m_outputPostions.emplace_back(100, 35);
}

void CircuitComparator::evaluate() {

    SignalType first = m_sig_in.checkInputSignalValue(0);
    if (first == SignalType::Not_Connected) {
	m_sig_out.putAll(first);
	return;
    }

    SignalType second = m_sig_in.checkInputSignalValue(1);
    if (second == SignalType::Not_Connected) {
	m_sig_out.putAll(second);
	return;
    }

    if (first != second) {
	m_sig_out.put(SignalType::False, 1);

	// slucaj G
	if (first == SignalType::True) {
	    m_sig_out.put(SignalType::True, 0);
	    m_sig_out.put(SignalType::False, 2);
	}
	// slucaj L
	else {
	    m_sig_out.put(SignalType::False, 0);
	    m_sig_out.put(SignalType::True, 2);
	}
    }

    // slucaj E
    else {
	m_sig_out.put(SignalType::False, 0);
	m_sig_out.put(SignalType::True, 1);
	m_sig_out.put(SignalType::False, 2);
    }
}

CircuitSRLatch::CircuitSRLatch()
    : MultiInputOutputCircuit(CircuitType::SRLatch, 2, 2) {
    m_inputPostions.emplace_back(0, 76);
    m_inputPostions.emplace_back(0, 43);
    m_outputPostions.emplace_back(100, 76);
    m_outputPostions.emplace_back(100, 43);
}

void CircuitSRLatch::evaluate() {
    SignalType first_input = m_sig_in.checkInputSignalValue(0);
    SignalType second_input = m_sig_in.checkInputSignalValue(1);

    // doesn't change state when both input signals are false
    if (first_input == SignalType::False && second_input == SignalType::False) {
	return;
    }

    if (first_input == SignalType::True && second_input == SignalType::False) {
	m_sig_out.put(SignalType::False, 1);
	m_sig_out.put(SignalType::True, 0);
    } else if (first_input == SignalType::False &&
	       second_input == SignalType::True) {
	m_sig_out.put(SignalType::False, 0);
	m_sig_out.put(SignalType::True, 1);
    } else if (first_input == SignalType::True &&
	       second_input == SignalType::True) {
    }
}

CircuitDLatch::CircuitDLatch()
    : MultiInputOutputCircuit(CircuitType::DLatch, 2, 2) {
    m_inputPostions.emplace_back(0, 76);
    m_inputPostions.emplace_back(0, 43);
    m_outputPostions.emplace_back(100, 76);
    m_outputPostions.emplace_back(100, 43);
}

void CircuitDLatch::evaluate() {
    // D
    SignalType first_input = m_sig_in.checkInputSignalValue(1);
    // enable
    SignalType second_input = m_sig_in.checkInputSignalValue(0);

    // doesn't change when enable is false
    if (first_input == SignalType::False) {
	return;
    }

    // on positive tick of the enable
    if (first_input == SignalType::True && second_input == SignalType::True) {
	m_sig_out.put(SignalType::False, 1);
	m_sig_out.put(SignalType::True, 0);
    } else if (first_input == SignalType::False &&
	       second_input == SignalType::True) {
	m_sig_out.put(SignalType::True, 1);
	m_sig_out.put(SignalType::False, 0);
    }
}

void Circuit::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    m_offset = event->pos();
    this->setSelected(true);
    this->setCursor(QCursor(Qt::CrossCursor));
}

void Circuit::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    this->setPos(mapToScene(event->pos() - m_offset));
}

void Circuit::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    this->setCursor(QCursor(Qt::ArrowCursor));
    Q_UNUSED(event);
}

SingleInputOutputCircuit::SingleInputOutputCircuit(CircuitType c)
    : Circuit(c) {}

MultiInputOutputCircuit::MultiInputOutputCircuit(CircuitType c,
						 unsigned u_in,
						 unsigned u_out)
    : Circuit(c), m_sig_out(u_out), m_sig_in(u_in) {}

void MultiInputCircuit::setConCoords() {

    // multiplexer je specijalan slucaj ovih kola, za njega ne postavljam
    // koordinate ovde
    if (this->getCircuitType() == CircuitType::Multiplex)
	return;
    m_outputPostions.emplace_back(45, 20);
    const auto num_of_inputs = getInputN();
    const auto step = 30.0 / (num_of_inputs + 1);
    for (unsigned i = 1; i <= num_of_inputs; i++) {
	m_inputPostions.emplace_back(0, step * i + 5);
    }
}

MultiInputCircuit::MultiInputCircuit(CircuitType c, unsigned u)
    : Circuit(c), m_sig_in(u) {
    setConCoords();
}

JustOutputCircuit::JustOutputCircuit(CircuitType c) : Circuit(c) {}

MultiOutputCircuit::MultiOutputCircuit(CircuitType c, unsigned u)
    : Circuit(c), m_sig_out(u) {}

CircuitIn::CircuitIn(bool s) : JustOutputCircuit(CircuitType::In) {
    m_sig.put(s ? SignalType::True : SignalType::False);
    m_outputPostions.emplace_back(QPoint(11, 0));
}

CircuitOut::CircuitOut()
    : JustInputCircuit(CircuitType::Out), m_signal(SignalType::Not_Connected) {
    m_inputPostions.emplace_back(QPoint(-15, 0));
}
