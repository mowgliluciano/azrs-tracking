#include <signal.hpp>

#ifdef __USE_UTIL__
#include <util.hpp>
#endif

Signal::~Signal() { put(SignalType::Not_Connected); }

void Signal::put(SignalType s) {
    if (s != m_val) {
	m_val = s;
	emit changedValue();
    }
}

auto Signal::negateValue() const -> SignalType {
    switch (m_val) {
    case SignalType::True: return SignalType::False;
    case SignalType::False: return SignalType::True;
    default: return SignalType::Not_Connected;
    }
}

SingleInput::SingleInput() { m_sig = std::make_shared<Signal>(); }

auto SingleInput::value() const -> SignalType {
    if (m_sig.expired())
	return SignalType::Not_Connected;
    return m_sig.lock()->value();
}

SingleOutput::SingleOutput() { m_sig = std::make_shared<Signal>(); }

auto MultiInput::hasSingalType(SignalType s,
			       unsigned begin,
			       unsigned end) const -> bool {
    if (!end)
	end = m_input_n;
#ifdef __USE_UTIL__
    if (end > m_input_n || begin >= end) {
	util::upozorenje(
	    std::string("MultiInput.hasSingalType: passed interval is [")
		.append(std::to_string(begin))
		.append(", ")
		.append(std::to_string(end))
		.append("), but instance has ")
		.append(std::to_string(m_input_n))
		.append(" input slots"));
	return false;
    }
#endif
    for (auto i = begin; i < end; i++) {
	if (checkInputSignalValue(i) == s)
	    return true;
    }
    return false;
}

auto MultiInput::prioritySignalSearcher(SignalType s1,
					      SignalType s2,
					      SignalType s3,
					      unsigned begin,
					      unsigned end) const -> SignalType {
    if (!end)
	end = m_input_n;
#ifdef __USE_UTIL__
    if (end > m_input_n || begin >= end) {
	util::upozorenje(
	    std::string(
		"MultiInput.prioritySignalSearcher: passed interval is [")
		.append(std::to_string(begin))
		.append(", ")
		.append(std::to_string(end))
		.append("), but instance has ")
		.append(std::to_string(m_input_n))
		.append(" input slots"));
	return SignalType::Not_Connected;
    }
#endif
    SignalType highestFound = s3;
    for (auto i = begin; i < end; i++) {
	SignalType current = checkInputSignalValue(i);
	if (current == s1)
	    return s1;
	if (current == s2)
	    highestFound = s2;
    }
    return highestFound;
}

auto MultiInput::checkInputSignalValue(unsigned u) const -> SignalType {
#ifdef __USE_UTIL__
    if (u >= m_input_n) {
	util::upozorenje(
	    std::string("MultiInput.checkInputSignalValue: instance has  ")
		.append(std::to_string(m_input_n))
		.append(" input slot, but ")
		.append(std::to_string(u))
		.append(" was passed"));
	return SignalType::Not_Connected;
    }
#endif
    return m_sig[u].value();
}

auto MultiInput::hasAllEqualSignalType(unsigned begin, unsigned end) const -> bool {
    if (!end)
	end = m_input_n;
#ifdef __USE_UTIL__
    if (end > m_input_n || begin >= end) {
	util::upozorenje(
	    std::string(
		"MultiInput.prioritySignalSearcher: passed interval is [")
		.append(std::to_string(begin))
		.append(", ")
		.append(std::to_string(end))
		.append("), but instance has ")
		.append(std::to_string(m_input_n))
		.append(" input slots"));
	return false;
    }
#endif
    SignalType sig = checkInputSignalValue(begin);
    for (auto i = begin + 1; i < end; i++) {
	if (checkInputSignalValue(i) != sig)
	    return false;
    }
    return true;
}

void MultiInput::putSignal(std::weak_ptr<Signal> sig, unsigned u) {
#ifdef __USE_UTIL__
    if (u >= m_input_n)
	util::upozorenje(std::string("MultiInput.putSignal: instance has  ")
			     .append(std::to_string(m_input_n))
			     .append(" input slot, but ")
			     .append(std::to_string(u))
			     .append(" was passed"));
    else
#endif
	m_sig[u].putSignal(sig);
}

auto MultiInput::getSignal(unsigned u) -> std::weak_ptr<Signal> {
#ifdef __USE_UTIL__
    if (u >= m_input_n)
	util::greska(std::string("MultiInput.getSignal: instance has  ")
			 .append(std::to_string(m_input_n))
			 .append(" input slot, but ")
			 .append(std::to_string(u))
			 .append(" was passed"));
#endif
    return m_sig[u].getSignal();
}

auto MultiInput::operator[](unsigned u) -> SingleInput & {
#ifdef __USE_UTIL__
    if (u >= m_input_n)
	util::greska(std::string("MultiInput.[]: instance has  ")
			 .append(std::to_string(m_input_n))
			 .append(" input slot, but ")
			 .append(std::to_string(u))
			 .append(" was passed"));
#endif
    return m_sig[u];
}

auto MultiOutput::getSignal(unsigned u) const -> std::weak_ptr<Signal> {
#ifdef __USE_UTIL__
    if (u >= m_output_n)
	util::greska(std::string("MultiOutput.getSignal: instance has  ")
			 .append(std::to_string(m_output_n))
			 .append(" output slot, but ")
			 .append(std::to_string(u))
			 .append(" was passed"));
#endif
    return m_sig[u].getSignal();
}

void MultiOutput::putAll(SignalType s, unsigned begin, unsigned end) {
    if (!end or end > m_output_n)
	end = m_output_n;
#ifdef __USE_UTIL__
    if (begin >= end)
	util::upozorenje(std::string("MultiOutput.putAll: passed interval is [")
			     .append(std::to_string(begin))
			     .append(", ")
			     .append(std::to_string(end))
			     .append("), but instance has ")
			     .append(std::to_string(m_output_n))
			     .append(" output slots"));
#endif
    for (auto i = begin; i < end; i++)
	m_sig[i].put(s);
}

void MultiOutput::put(SignalType s, unsigned u) {
#ifdef __USE_UTIL__
    if (u >= m_output_n)
	util::upozorenje(std::string("MultiOutput.put: instance has  ")
			     .append(std::to_string(m_output_n))
			     .append(" output slot, but ")
			     .append(std::to_string(u))
			     .append(" was passed"));
    else
#endif
	m_sig[u].put(s);
}

auto MultiOutput::value(unsigned u) const -> SignalType {
#ifdef __USE_UTIL__
    if (u < m_output_n) {
	util::upozorenje(std::string("MultiOutput.value: instance has  ")
			     .append(std::to_string(m_output_n))
			     .append(" output slot, but ")
			     .append(std::to_string(u))
			     .append(" was passed"));
	return SignalType::Not_Connected;
    } else
#endif
	return m_sig[u].value();
}
