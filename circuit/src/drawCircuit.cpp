#include <QPainter>
#include <circuit.hpp>
#include <cmath>
// Specific circuit drawing  implementations

auto Circuit::SignalType2Color(SignalType s) const -> QColor {
    switch (s) {
    case SignalType::Not_Connected: return Qt::gray;
    case SignalType::True: return Qt::green;
    case SignalType::False: return Qt::red;
    default: return Qt::black;
    }
}

void CircuitId::paint(QPainter *painter,
		      const QStyleOptionGraphicsItem *,
		      QWidget *) {
    painter->setPen(m_pen);
    painter->drawLine(-10, -10, 10, 0);
    painter->drawLine(-10, -10, -10, 10);
    painter->drawLine(-10, 10, 10, 0);

    painter->drawLine(-19, 0, -10, 0);

    painter->drawLine(22, 0, 10, 0);

    setRect(-22, -11, 47, 25);
}

void CircuitAnd::paint(QPainter *painter,
		       const QStyleOptionGraphicsItem *,
		       QWidget *) {
    painter->setPen(m_pen);
    painter->drawLine(5, 5, 30, 5);
    painter->drawLine(5, 5, 5, 35);
    painter->drawLine(5, 35, 30, 35);
    painter->drawArc(QRect(20, 5, 20, 30), -90 * 16, 180 * 16);

    const auto num_of_inputs = getInputN();
    const auto step = 30.0 / (num_of_inputs + 1);
    for (unsigned i = 1; i <= num_of_inputs; i++) {
	painter->drawLine(0, step * i + 5, 5, step * i + 5);
	//        m_inputPostions.push_back(QPointF(0, step * i + 5));
    }

    painter->drawLine(40, 20, 45, 20);
    //    m_outputPostions.push_back(QPointF(45,20));
    setRect(-1, 0, 50, 40);
}

void CircuitOr::paint(QPainter *painter,
		      const QStyleOptionGraphicsItem *,
		      QWidget *) {
    painter->setPen(m_pen);
    painter->drawArc(QRect(0, 5, 10, 30), -90 * 16, 180 * 15);
    painter->drawLine(5, 5, 19, 5);
    painter->drawLine(5, 35, 17, 35);

    painter->drawArc(QRect(-5, 5, 45, 30), 0, 90 * 15);
    painter->drawArc(QRect(-5, 5, 45, 30), -90 * 16, 90 * 15);
    const auto numOfInputs = getInputN();
    const auto step = 30.0 / (numOfInputs + 1);
    for (unsigned i = 1; i <= numOfInputs; i++) {
	painter->drawLine(0, step * i + 5, 8, step * i + 5);
    }

    painter->drawLine(40, 20, 45, 20);

    setRect(-1, -1, 50, 40);
}

void CircuitXor::paint(QPainter *painter,
		       const QStyleOptionGraphicsItem *,
		       QWidget *) {
    painter->setPen(m_pen);
    painter->drawArc(QRect(-2, 5, 8, 30), -90 * 16, 180 * 16);
    painter->drawArc(QRect(0, 5, 10, 30), -90 * 16, 180 * 16);
    painter->drawLine(5, 5, 17, 5);
    painter->drawLine(5, 35, 17, 35);

    painter->drawArc(QRect(-5, 5, 45, 30), 0, 90 * 16);
    painter->drawArc(QRect(-5, 5, 45, 30), -90 * 16, 90 * 16);

    const auto numOfInputs = getInputN();
    const auto step = 30.0 / (numOfInputs + 1);
    for (unsigned i = 1; i <= numOfInputs; i++) {
	painter->drawLine(0, step * i + 5, 5, step * i + 5);
    }

    painter->drawLine(40, 20, 45, 20);

    setRect(-3, 0, 50, 40);
}

void CircuitNon::paint(QPainter *painter,
		       const QStyleOptionGraphicsItem *,
		       QWidget *) {
    painter->setPen(m_pen);
    painter->drawLine(-10, -10, 10, 0);
    painter->drawLine(-10, -10, -10, 10);
    painter->drawLine(-10, 10, 10, 0);

    painter->drawLine(-19, 0, -10, 0);

    QPointF center = QPointF(13, 0);
    painter->drawEllipse(center, 3, 3);

    painter->drawLine(16, 0, 22, 0);

    setRect(-22, -11, 48, 25);
}

void CircuitNand::paint(QPainter *painter,
			const QStyleOptionGraphicsItem *,
			QWidget *) {

    painter->setPen(m_pen);
    painter->drawLine(5, 5, 30, 5);
    painter->drawLine(5, 5, 5, 35);
    painter->drawLine(5, 35, 30, 35);
    painter->drawArc(QRect(20, 5, 20, 30), -90 * 16, 180 * 16);

    const auto numOfInputs = getInputN();
    const auto step = 30.0 / (numOfInputs + 1);
    for (unsigned i = 1; i <= numOfInputs; i++) {
	painter->drawLine(0, step * i + 5, 5, step * i + 5);
    }

    QPointF center = QPointF(44, 20);
    painter->drawEllipse(center, 3, 3);

    painter->drawLine(47, 20, 55, 20);
    setRect(-3, 0, 58, 45);
}

void CircuitNor::paint(QPainter *painter,
		       const QStyleOptionGraphicsItem *,
		       QWidget *) {
    painter->setPen(m_pen);
    painter->drawArc(QRect(0, 5, 10, 30), -90 * 16, 180 * 16);
    painter->drawLine(5, 5, 17, 5);
    painter->drawLine(5, 35, 17, 35);

    painter->drawArc(QRect(-5, 5, 45, 30), 0, 90 * 16);
    painter->drawArc(QRect(-5, 5, 45, 30), -90 * 16, 90 * 16);

    const auto numpOfInputs = getInputN();
    const auto step = 30.0 / (numpOfInputs + 1);
    for (unsigned i = 1; i <= numpOfInputs; i++) {
	painter->drawLine(0, step * i + 5, 8, step * i + 5);
    }

    QPointF center = QPointF(43, 20);
    painter->drawEllipse(center, 3, 3);

    painter->drawLine(46, 20, 55, 20); // 49

    setRect(-3, 0, 58, 55); // 50,45
}

void CircuitNxor::paint(QPainter *painter,
			const QStyleOptionGraphicsItem *,
			QWidget *) {
    painter->setPen(m_pen);
    painter->drawArc(QRect(-2, 5, 8, 30), -90 * 16, 180 * 16);
    painter->drawArc(QRect(0, 5, 10, 30), -90 * 16, 180 * 16);
    painter->drawLine(5, 5, 17, 5);
    painter->drawLine(5, 35, 17, 35);

    painter->drawArc(QRect(-5, 5, 45, 30), 0, 90 * 16);
    painter->drawArc(QRect(-5, 5, 45, 30), -90 * 16, 90 * 16);

    const auto numOfInputs = getInputN();
    const auto step = 30.0 / (numOfInputs + 1);
    for (unsigned i = 1; i <= numOfInputs; i++) {
	painter->drawLine(0, step * i + 5, 5, step * i + 5);
    }

    QPointF center = QPointF(43, 20);
    painter->drawEllipse(center, 3, 3);

    painter->drawLine(46, 20, 55, 20);

    setRect(-3, 0, 59, 40);
}

void Multiplexer::paint(QPainter *painter,
			const QStyleOptionGraphicsItem *,
			QWidget *) {
    painter->setPen(m_pen);

    int height = (m_inputSlots + 1) * 20;
    int width = (m_controlSlots + 1) * 24;
    painter->drawRect(10, 10, width, height);

    QFont font = painter->font();
    font.setPixelSize(12);
    font.bold();
    painter->setFont(font);

    unsigned i;
    int x, y;
    for (i = 1; i <= m_inputSlots; i++) {
	y = height / (m_inputSlots + 1) * i + 10;
	painter->drawLine(0, y, 10, y);
	// numbers
	auto printable = QStringLiteral("%1").arg(m_inputSlots - i);
	painter->drawText(QPoint(13, y + 4), printable);
    }

    for (i = 1; i <= m_controlSlots; i++) {
	x = width / (m_controlSlots + 1) * i + 10;
	painter->drawLine(x, height + 10, x, height + 20);
	auto printable = QStringLiteral("%1").arg(i - 1);
	painter->drawText(QPoint(x - 4, height + 6), printable);
    }

    painter->drawLine(width + 10, height / 2 + 10, width + 20, height / 2 + 10);

    const QRect rectangle = QRect(
	(width - 10) * 3 / 7, (height - 10 - m_inputSlots) * 5 / 9, 40, 20);
    painter->drawText(rectangle, Qt::AlignCenter, "MUX");

    setRect(-3, 0, width + 23, height + 23);
}

void Demultiplexer::paint(QPainter *painter,
			  const QStyleOptionGraphicsItem *,
			  QWidget *) {
    painter->setPen(m_pen);

    const auto numOfOutputs = getOutputN();
    int height = (numOfOutputs + 1) * 20;
    int width = (m_controlSlots + 1) * 24;
    painter->drawRect(10, 10, width, height);

    painter->drawLine(0, height / 2 + 10, 10, height / 2 + 10);

    QFont font = painter->font();
    font.setPixelSize(12);
    font.bold();
    painter->setFont(font);

    unsigned i;
    int x, y;
    for (i = 1; i <= numOfOutputs; i++) {
	y = height / (numOfOutputs + 1) * i + 10;
	painter->drawLine(width + 10, y, width + 20, y);
	auto printable = QStringLiteral("%1").arg(numOfOutputs - i);
	painter->drawText(QPoint(width - m_controlSlots, y + 4), printable);
    }

    for (i = 1; i <= m_controlSlots; i++) {
	x = width / (m_controlSlots + 1) * i + 10;
	painter->drawLine(x, height + 10, x, height + 20);
	auto printable = QStringLiteral("%1").arg(i - 1);
	painter->drawText(QPoint(x - 4, height + 6), printable);
    }

    const QRect rectangle = QRect(
	(width - 10) * 3 / 7, (height - 10 - numOfOutputs) * 5 / 9, 40, 20);
    painter->drawText(rectangle, Qt::AlignCenter, "DEMUX");

    setRect(-3, 0, width + 23, height + 23);
}

void CircuitEncoder::paint(QPainter *painter,
			   const QStyleOptionGraphicsItem *,
			   QWidget *) {
    painter->setPen(m_pen);

    const auto numOfInputs = getInputN();
    const auto numOfOutputs = getOutputN();

    int height = (numOfInputs + 1) * 20;
    int width = 80;
    painter->drawRect(10, 10, width, height);

    QFont font = painter->font();
    font.setPixelSize(12);
    font.bold();
    painter->setFont(font);

    unsigned i;
    int y;
    for (i = 1; i <= numOfInputs; i++) {
	y = height / (numOfInputs + 1) * i + 10;
	painter->drawLine(0, y, 10, y);
	auto printable = QStringLiteral("%1").arg(numOfInputs - i);
	painter->drawText(QPoint(13, y + 4), printable);
    }

    for (i = 1; i <= numOfOutputs; i++) {
	y = height / (numOfOutputs + 1) * i + 10;
	painter->drawLine(width + 10, y, width + 20, y);
	auto printable = QStringLiteral("%1").arg(numOfOutputs - i);
	painter->drawText(QPoint(width - numOfOutputs, y + 4), printable);
    }

    const QRect rectangle = QRect(
	(width - 10) * 3 / 7, (height - 10 - numOfInputs) * 5 / 9, 40, 20);
    painter->drawText(rectangle, Qt::AlignCenter, "ENC");

    setRect(-3, 0, width + 23, height + 23);
}

void CircuitDecoder::paint(QPainter *painter,
			   const QStyleOptionGraphicsItem *,
			   QWidget *) {
    painter->setPen(m_pen);

    const auto numOfInputs = getInputN();
    const auto numOfOutputs = getOutputN();

    int height = (numOfOutputs + 1) * 20;
    int width = 80;
    painter->drawRect(10, 10, width, height);

    QFont font = painter->font();
    font.setPixelSize(12);
    font.bold();
    painter->setFont(font);

    unsigned i;
    int y;
    for (i = 1; i <= numOfInputs; i++) {
	y = height / (numOfInputs + 1) * i + 10;
	painter->drawLine(0, y, 10, y);
	auto printable = QStringLiteral("%1").arg(numOfInputs - i);
	painter->drawText(QPoint(13, y + 4), printable);
    }

    for (i = 1; i <= numOfOutputs; i++) {
	y = height / (numOfOutputs + 1) * i + 10;
	painter->drawLine(width + 10, y, width + 20, y);
	auto printable = QStringLiteral("%1").arg(numOfOutputs - i);
	painter->drawText(QPoint(80 - numOfInputs, y + 4), printable);
    }

    // TODO:Sredina
    const QRect rectangle = QRect(
	(width - 10) * 3 / 7, (height - 10 - numOfOutputs) * 5 / 9, 40, 20);
    painter->drawText(rectangle, Qt::AlignCenter, "DEC");

    setRect(-3, 0, width + 23, height + 23);
}

// POLUsabirac i POLUoduzimac
void CircuitHalfAdder::paint(QPainter *painter,
			     const QStyleOptionGraphicsItem *,
			     QWidget *) {
    painter->setPen(m_pen);
    painter->drawRect(10, 10, 80, 100);

    int i, y;
    // 2 ulaza
    for (i = 1; i <= 2; i++) {
	y = 100 / (3) * i + 10; // br.ulaza+1
	painter->drawLine(0, y, 10, y);
    }

    // 2 izlaza
    for (i = 1; i <= 2; i++) {
	y = 100 / (3) * i + 10;
	painter->drawLine(90, y, 100, y);
    }

    QFont font = painter->font();
    font.setPixelSize(13);
    font.bold();
    painter->setFont(font);
    const QRect rectangle = QRect(30, 50, 40, 20);
    painter->drawText(rectangle, Qt::AlignCenter, "HADD");

    setRect(-6, 0, 106, 120);
}

// Sabirac i oduzimac
void CircuitAdder::paint(QPainter *painter,
			 const QStyleOptionGraphicsItem *,
			 QWidget *) {
    painter->setPen(m_pen);
    painter->drawRect(10, 10, 80, 100);

    int i, y;

    for (i = 1; i <= 3; i++) {
	y = 100 / (4) * i + 10; // br.ulaza +1
	painter->drawLine(0, y, 10, y);
    }

    // 2 izlaza
    for (i = 1; i <= 2; i++) {
	y = 100 / (3) * i + 10;
	painter->drawLine(90, y, 100, y);
    }

    QFont font = painter->font();
    font.setPixelSize(13);
    font.bold();
    painter->setFont(font);
    const QRect rectangle = QRect(30, 50, 40, 20);
    painter->drawText(rectangle, Qt::AlignCenter, "ADD");

    setRect(-6, 0, 106, 120);
}

// crtanje poluoduzimaca
void CircuitHalfSubtractor::paint(QPainter *painter,
				  const QStyleOptionGraphicsItem *,
				  QWidget *) {
    painter->setPen(m_pen);
    painter->drawRect(10, 10, 80, 100);

    int i, y;
    // 2 ulaza
    for (i = 1; i <= 2; i++) {
	y = 100 / (3) * i + 10; // br.ulaza+1
	painter->drawLine(0, y, 10, y);
    }

    // 2 izlaza
    for (i = 1; i <= 2; i++) {
	y = 100 / (3) * i + 10;
	painter->drawLine(90, y, 100, y);
    }

    QFont font = painter->font();
    font.setPixelSize(13);
    font.bold();
    painter->setFont(font);
    const QRect rectangle = QRect(30, 50, 40, 20);
    painter->drawText(rectangle, Qt::AlignCenter, "HSUB");

    setRect(-6, 0, 106, 120);
}

// crtanje oduzimaca
void CircuitSubtractor::paint(QPainter *painter,
			      const QStyleOptionGraphicsItem *,
			      QWidget *) {
    painter->setPen(m_pen);
    painter->drawRect(10, 10, 80, 100);

    int i, y;

    for (i = 1; i <= 3; i++) {
	y = 100 / (4) * i + 10; // br.ulaza +1
	painter->drawLine(0, y, 10, y);
    }

    // 2 izlaza
    for (i = 1; i <= 2; i++) {
	y = 100 / (3) * i + 10;
	painter->drawLine(90, y, 100, y);
    }

    QFont font = painter->font();
    font.setPixelSize(13);
    font.bold();
    painter->setFont(font);
    const QRect rectangle = QRect(30, 50, 40, 20);
    painter->drawText(rectangle, Qt::AlignCenter, "SUB");

    setRect(-6, 0, 106, 120);
}

void CircuitClock::paint(QPainter *painter,
			 const QStyleOptionGraphicsItem *,
			 QWidget *) {
    painter->setPen(m_pen);
    painter->drawRect(-15, -15, 30, 30);

    QFont font = painter->font();
    font.setPixelSize(15);
    font.bold();

    painter->setFont(font);
    QPointF center = QPointF(0, 0);
    painter->drawEllipse(center, 7, 7);
    QPointF start = QPointF(15, 0);
    QPointF end = QPointF(20, 0);
    painter->drawLine(start, end);

    painter->setPen(SignalType2Color(m_sig.value()));
    painter->drawLine(center, getBig());
    painter->drawLine(center, getSmall());
    setRect(-16, -16, 37, 37);
}

void CircuitSpliter::paint(QPainter *painter,
			   const QStyleOptionGraphicsItem *,
			   QWidget *) {

    painter->setPen(m_pen);
    painter->drawRect(-10, -10, 20, 20);

    painter->drawLine(QPointF(-10, 3), QPointF(-7, 0));
    painter->drawLine(QPointF(-10, -3), QPointF(-7, 0));

    painter->drawLine(QPointF(10, 3), QPointF(13, 0));
    painter->drawLine(QPointF(10, -3), QPointF(13, 0));

    painter->drawLine(QPointF(-3, 10), QPointF(0, 13));
    painter->drawLine(QPointF(3, 10), QPointF(0, 13));

    painter->drawLine(QPointF(-3, -10), QPointF(0, -13));
    painter->drawLine(QPointF(3, -10), QPointF(0, -13));

    painter->drawLine(QPointF(-15, 0), QPointF(-10, 0));
    painter->drawLine(QPointF(15, 0), QPointF(13, 0));
    painter->drawLine(QPointF(0, -15), QPointF(0, -13));
    painter->drawLine(QPointF(0, 15), QPointF(0, 13));

    setRect(-16, -16, 32, 32);
}

void JKFlipFlop::paint(QPainter *painter,
		       const QStyleOptionGraphicsItem *,
		       QWidget *) {
    painter->setPen(m_pen);
    painter->drawRect(10, 10, 80, 100);

    painter->drawLine(0, 30, 10, 30);
    painter->drawLine(0, 90, 10, 90);
    painter->drawLine(0, 60, 10, 60);

    painter->drawLine(90, 40, 100, 40);
    painter->drawLine(90, 80, 100, 80);

    QFont font = painter->font();
    font.setPixelSize(14);
    font.bold();
    painter->setFont(font);

    const QRect rectangle1 = QRect(15, 22, 15, 15);
    painter->drawText(rectangle1, Qt::AlignCenter, "J");

    const QRect rectangle2 = QRect(15, 82, 15, 15);
    painter->drawText(rectangle2, Qt::AlignCenter, "K");

    const QRect rectangle3 = QRect(15, 52, 25, 15);
    painter->drawText(rectangle3, Qt::AlignCenter, "clk");

    const QRect rectangle4 = QRect(75, 32, 15, 15);
    painter->drawText(rectangle4, Qt::AlignCenter, "Q");

    const QRect rectangle5 = QRect(75, 72, 15, 15);
    painter->drawText(rectangle5, Qt::AlignCenter, "!Q");

    setRect(-6, 0, 106, 120);
}

void DFlipFlop::paint(QPainter *painter,
		      const QStyleOptionGraphicsItem *,
		      QWidget *) {
    painter->setPen(m_pen);
    painter->drawRect(10, 10, 80, 100);

    painter->drawLine(0, 30, 10, 30);
    painter->drawLine(0, 60, 10, 60);

    painter->drawLine(90, 40, 100, 40);
    painter->drawLine(90, 80, 100, 80);

    QFont font = painter->font();
    font.setPixelSize(14);
    font.bold();
    painter->setFont(font);

    const QRect rectangle1 = QRect(15, 22, 15, 15);
    painter->drawText(rectangle1, Qt::AlignCenter, "D");

    const QRect rectangle3 = QRect(15, 52, 25, 15);
    painter->drawText(rectangle3, Qt::AlignCenter, "clk");

    const QRect rectangle4 = QRect(75, 32, 15, 15);
    painter->drawText(rectangle4, Qt::AlignCenter, "Q");

    const QRect rectangle5 = QRect(75, 72, 15, 15);
    painter->drawText(rectangle5, Qt::AlignCenter, "!Q");

    setRect(-6, 0, 106, 120);
}

void TFlipFlop::paint(QPainter *painter,
		      const QStyleOptionGraphicsItem *,
		      QWidget *) {
    painter->setPen(m_pen);
    painter->drawRect(10, 10, 80, 100);

    painter->drawLine(0, 30, 10, 30);
    painter->drawLine(0, 60, 10, 60);

    painter->drawLine(90, 40, 100, 40);
    painter->drawLine(90, 80, 100, 80);

    QFont font = painter->font();
    font.setPixelSize(14);
    font.bold();
    painter->setFont(font);

    const QRect rectangle1 = QRect(15, 22, 15, 15);
    painter->drawText(rectangle1, Qt::AlignCenter, "T");

    const QRect rectangle3 = QRect(15, 52, 25, 15);
    painter->drawText(rectangle3, Qt::AlignCenter, "clk");

    const QRect rectangle4 = QRect(75, 32, 15, 15);
    painter->drawText(rectangle4, Qt::AlignCenter, "Q");

    const QRect rectangle5 = QRect(75, 72, 15, 15);
    painter->drawText(rectangle5, Qt::AlignCenter, "!Q");

    setRect(-6, 0, 106, 120);
}

void SRFlipFlop::paint(QPainter *painter,
		       const QStyleOptionGraphicsItem *,
		       QWidget *) {
    painter->setPen(m_pen);
    painter->drawRect(10, 10, 80, 100);

    painter->drawLine(0, 30, 10, 30);
    painter->drawLine(0, 90, 10, 90);
    painter->drawLine(0, 60, 10, 60);

    painter->drawLine(90, 40, 100, 40);
    painter->drawLine(90, 80, 100, 80);

    QFont font = painter->font();
    font.setPixelSize(14);
    font.bold();
    painter->setFont(font);

    const QRect rectangle1 = QRect(15, 22, 15, 15);
    painter->drawText(rectangle1, Qt::AlignCenter, "S");

    const QRect rectangle2 = QRect(15, 82, 15, 15);
    painter->drawText(rectangle2, Qt::AlignCenter, "R");

    const QRect rectangle3 = QRect(15, 52, 25, 15);
    painter->drawText(rectangle3, Qt::AlignCenter, "clk");

    const QRect rectangle4 = QRect(75, 32, 15, 15);
    painter->drawText(rectangle4, Qt::AlignCenter, "Q");

    const QRect rectangle5 = QRect(75, 72, 15, 15);
    painter->drawText(rectangle5, Qt::AlignCenter, "!Q");

    setRect(-6, 0, 106, 120);
}

void CircuitComparator::paint(QPainter *painter,
			      const QStyleOptionGraphicsItem *,
			      QWidget *) {
    painter->setPen(m_pen);
    painter->drawRect(10, 10, 80, 100);

    int i, y;
    // 2 ulaza
    for (i = 1; i <= 2; i++) {
	y = 100 / (3) * i + 10; // br.ulaza+1
	painter->drawLine(0, y, 10, y);
    }

    // 3 izlaza
    for (i = 1; i <= 3; i++) {
	y = 100 / (4) * i + 10;
	painter->drawLine(90, y, 100, y);
    }

    QFont font = painter->font();
    font.setPixelSize(9);
    font.bold();
    painter->setFont(font);
    const QRect rectangle = QRect(30, 50, 40, 20);
    painter->drawText(rectangle, Qt::AlignCenter, "CMP");

    setRect(-6, 0, 106, 120);
}

void CircuitIn::paint(QPainter *painter,
		      const QStyleOptionGraphicsItem *,
		      QWidget *) {
    painter->setPen(m_pen);

    painter->setBrush(SignalType2Color(m_sig.value()));

    int dim = 20;
    int pos = -(dim / 2);
    int fix = 3;
    painter->drawEllipse(QRect(pos, pos, dim, dim));

    painter->setBrush(Qt::black);
    painter->drawLine(-pos - (fix - 1), 0, -pos + (fix - 1), 0);
    setRect(pos - fix, pos - fix, dim + fix * 2, dim + fix * 2);
}

void CircuitOut::paint(QPainter *painter,
		       const QStyleOptionGraphicsItem *,
		       QWidget *) {
    painter->setPen(m_pen);

    painter->setBrush(SignalType2Color(value()));

    int dim = 30;
    int pos = -(dim / 2);
    int fix = 2;
    painter->drawEllipse(QRect(pos, pos, dim, dim));

    painter->setBrush(Qt::black);
    painter->drawLine(pos - (fix - 1), 0, pos + (fix - 1), 0);
    setRect(pos - fix, pos - fix, dim + fix * 2, dim + fix * 2);
}

void CircuitDLatch::paint(QPainter *painter,
			  const QStyleOptionGraphicsItem *,
			  QWidget *) {
    painter->setPen(m_pen);
    painter->drawRect(10, 10, 80, 100);

    int i, y;
    // 2 ulaza
    for (i = 1; i <= 2; i++) {
	y = 100 / (3) * i + 10;
	painter->drawLine(0, y, 10, y);
    }

    // 2 izlaza
    for (i = 1; i <= 2; i++) {
	y = 100 / (3) * i + 10;
	painter->drawLine(90, y, 100, y);
    }

    QFont font = painter->font();
    font.setPixelSize(13);
    font.bold();
    painter->setFont(font);
    const QRect rectangle = QRect(30, 50, 40, 20);
    painter->drawText(rectangle, Qt::AlignCenter, "Dlatch");

    setRect(0, 0, 100, 120);
}

void CircuitSRLatch::paint(QPainter *painter,
			   const QStyleOptionGraphicsItem *,
			   QWidget *) {
    painter->setPen(m_pen);
    painter->drawRect(10, 10, 80, 100);

    int i, y;
    // 2 ulaza
    for (i = 1; i <= 2; i++) {
	y = 100 / (3) * i + 10;
	painter->drawLine(0, y, 10, y);
    }

    // 2 izlaza
    for (i = 1; i <= 2; i++) {
	y = 100 / (3) * i + 10;
	painter->drawLine(90, y, 100, y);
    }

    QFont font = painter->font();
    font.setPixelSize(13);
    font.bold();
    painter->setFont(font);
    const QRect rectangle = QRect(30, 50, 40, 20);
    painter->drawText(rectangle, Qt::AlignCenter, "SRlatch");

    setRect(0, 0, 100, 120);
}

void MultiCircuit::paint(QPainter *painter,
			 const QStyleOptionGraphicsItem *,
			 QWidget *) {
    painter->setPen(m_pen);

    QFontMetrics fm(painter->font());

    int width = fm.width(m_name) + 2;
//    int didth = 40;
    if (width % 2)
     width++;

    int height = (std::max(getInputN(), getOutputN()) + 1) * 15;

    QRect frame(-width / 2, -height / 2, width, height);
    painter->drawRect(frame);

    int point = -height / 2, step = height / (getInputN() + 1);
    bool access_point_change = m_inputPostions.size() != getInputN() ||
			       m_outputPostions.size() != getOutputN();

    if (access_point_change)
	m_inputPostions.clear();
    for (unsigned i = 0; i < getInputN(); i++) {
	point += step;
	if (access_point_change)
	    m_inputPostions.emplace_back(QPoint(-width / 2 - 5, point));
	painter->drawLine(-width / 2 - 5, point, -width / 2, point);
    }

    point = -height / 2, step = height / (getOutputN() + 1);
    if (access_point_change)
	m_outputPostions.clear();
    for (unsigned i = 0; i < getOutputN(); i++) {
	point += step;
	if (access_point_change)
	    m_outputPostions.emplace_back(QPoint(width / 2 + 5, point));
	painter->drawLine(width / 2 + 5, point, width / 2, point);
    }

    painter->drawText(frame, Qt::AlignCenter, m_name);

    setRect(-width / 2 - 7, -height / 2 - 1, width + 14, height + 2);
}
