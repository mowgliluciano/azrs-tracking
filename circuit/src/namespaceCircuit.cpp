#include <circuitutil.hpp>
#include <cmath>
// Base implementations

void circuit::connect(Circuit &in,
		      unsigned in_n,
		      Circuit &out,
		      unsigned out_n) {
    out.connectInput(in.getSignal(in_n), out_n);
}

void circuit::connect(Circuit *in,
		      unsigned in_n,
		      Circuit *out,
		      unsigned out_n) {
    out->connectInput(in->getSignal(in_n), out_n);
}

void circuit::connect(std::shared_ptr<Circuit> &in,
		      unsigned in_n,
		      std::shared_ptr<Circuit> &out,
		      unsigned out_n) {
    out->connectInput(in->getSignal(in_n), out_n);
}

void circuit::connect(std::unique_ptr<Circuit> &in,
		      unsigned in_n,
		      std::unique_ptr<Circuit> &out,
		      unsigned out_n) {
    out->connectInput(in->getSignal(in_n), out_n);
}

void circuit::disconnect(Circuit &c, unsigned c_n) {
    c.connectInput(std::make_shared<Signal>(), c_n);
}

void circuit::disconnect(Circuit *c, unsigned c_n) {
    c->connectInput(std::make_shared<Signal>(), c_n);
}

void circuit::disconnect(std::shared_ptr<Circuit> &c, unsigned c_n) {
    c->connectInput(std::make_shared<Signal>(), c_n);
}

void circuit::disconnect(std::unique_ptr<Circuit> &c, unsigned c_n) {
    c->connectInput(std::make_shared<Signal>(), c_n);
}

auto circuit::getElementByType(CircuitType t, int numOfInput) -> Circuit * {
    switch (t) {
    case CircuitType::In: return new CircuitIn();
    case CircuitType::Out: return new CircuitOut();
    case CircuitType::Clock: return new CircuitClock();
    case CircuitType::Id: return new CircuitId();
    case CircuitType::And: return new CircuitAnd(numOfInput);
    case CircuitType::Or: return new CircuitOr(numOfInput);
    case CircuitType::Xor: return new CircuitXor(numOfInput);
    case CircuitType::Non: return new CircuitNon();
    case CircuitType::Nand: return new CircuitNand(numOfInput);
    case CircuitType::Nor: return new CircuitNor(numOfInput);
    case CircuitType::Nxor: return new CircuitNxor(numOfInput);
    case CircuitType::Spliter: return new CircuitSpliter();
    case CircuitType::Multiplex:
	return new Multiplexer(std::pow(2, numOfInput), numOfInput);
    case CircuitType::Demultiplex: return new Demultiplexer(1, numOfInput);
    case CircuitType::Encoder:
	return new CircuitEncoder(std::pow(2, numOfInput), numOfInput);
    case CircuitType::Decoder:
	return new CircuitDecoder(numOfInput, std::pow(2, numOfInput));
    case CircuitType::HalfAdder: return new CircuitHalfAdder();
    case CircuitType::Adder: return new CircuitAdder();
    case CircuitType::HalfSubtractor: return new CircuitHalfSubtractor();
    case CircuitType::Subtractor: return new CircuitSubtractor();
    case CircuitType::Comparator: return new CircuitComparator();
    case CircuitType::TFlipFlop: return new TFlipFlop();
    case CircuitType::DFlipFlop: return new DFlipFlop();
    case CircuitType::JKFlipFlop: return new JKFlipFlop();
    case CircuitType::SRFlipFlop: return new SRFlipFlop();
    case CircuitType::SRLatch: return new CircuitSRLatch();
    case CircuitType::DLatch: return new CircuitDLatch();
    case CircuitType::MultiCircuit: return new MultiCircuit();
    default: return new CircuitId();
    }
}
