#include <QFileInfo>
#include <algorithm>
#include <circuitutil.hpp>
#include <cmath>
#include <fstream>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#ifdef __USE_UTIL__
#include <util.hpp>
#endif

/////////////////////////////////////////////////////////
//
// Helper classes SmartReader and SmartWriter for parsing
//
/////////////////////////////////////////////////////////

namespace {
const QString extension = ".circuit";

enum TokenType { END = 0, BLOCK = 1, CIRCUIT = 2, NUMBER = 3 };

enum BlockType { IN = 1, OUT = 2, INNER = 3, CONNECT = 4 };

class TempBox {
  private:
    MultiCircuit *m_mc;

  public:
    TempBox(MultiCircuit *mc = nullptr) : m_mc(mc) {}
    ~TempBox() {
	if (m_mc)
	    delete m_mc;
    }
    inline auto get() -> MultiCircuit * { return m_mc; }
    auto move() -> MultiCircuit * {
	auto temp = m_mc;
	m_mc = nullptr;
	return temp;
    }
};

auto findPos(const std::vector<Circuit *> &v, const Circuit *c) -> unsigned {
    return std::find(v.cbegin(), v.cend(), c) - v.cbegin();
}

class SmartWriter {
  private:
    static const unsigned MAX_BUFFER_LEN = 4096;
    std::vector<char> m_buffer;
    std::ofstream m_file;
    bool global_export{false}, done{false};

  public:
    SmartWriter()  {}

    SmartWriter(const std::string &file_name)
	: m_file(file_name, std::ios::out | std::ios::binary)
	, global_export(true)
	, done(false) {}

    ~SmartWriter() { flush(); }

    auto flush() -> SmartWriter & {
	if (global_export) {
	    unsigned size = m_buffer.size();
	    if (!size)
		return *this;
	    char *temp_char_buffer = getWrittenData();
	    m_file.write(temp_char_buffer, size);
	    delete[] temp_char_buffer;
	    m_buffer.clear();
	}
	return *this;
    }

    auto put(unsigned val) -> SmartWriter & {
	if (!done) {
	    if (global_export && m_buffer.size() + 2 > MAX_BUFFER_LEN)
		flush();
	    m_buffer.push_back(TokenType::NUMBER);
	    m_buffer.push_back(val);
	}
	return *this;
    }

    auto put(CircuitType val) -> SmartWriter & {
	if (!done) {
	    if (global_export && m_buffer.size() + 2 > MAX_BUFFER_LEN)
		flush();
	    m_buffer.push_back(TokenType::CIRCUIT);
	    m_buffer.push_back(static_cast<unsigned>(val));
	}
	return *this;
    }

    auto put(BlockType val) -> SmartWriter & {
	if (!done) {
	    if (global_export && m_buffer.size() + 2 > MAX_BUFFER_LEN)
		flush();
	    m_buffer.push_back(TokenType::BLOCK);
	    m_buffer.push_back(static_cast<unsigned>(val));
	}
	return *this;
    }

    void end() {
	if (!done) {
	    m_buffer.push_back(TokenType::END);
	    m_buffer.push_back(0);
	    flush();
	    done = true;
	}
    }

    auto getWrittenData() -> char * {
	const unsigned size = m_buffer.size();
	if (size == 0)
	    return nullptr;
	char *temp_char_buffer = new char[size];
	for (unsigned i = 0; i < size; i++) {
	    temp_char_buffer[i] = m_buffer[i];
	}
	return temp_char_buffer;
    }

    auto getWrittenData(unsigned &size) -> char * {
	size = m_buffer.size();
	if (size == 0)
	    return nullptr;
	char *temp_char_buffer = new char[size];
	for (unsigned i = 0; i < size; i++) {
	    temp_char_buffer[i] = m_buffer[i];
	}
	return temp_char_buffer;
    }

    void save(const std::vector<Circuit *> &inputs,
	      const std::vector<Circuit *> &outputs,
	      const std::vector<Circuit *> &inner_circuits,
	      const std::vector<Connection *> &connections) {
	// writing number of inputs and outputs
	put(BlockType::IN).put(inputs.size());
	put(BlockType::OUT).put(outputs.size());

	// writing data on all the circuits
	if (!inner_circuits.empty()) {
	    put(BlockType::INNER);
	    for (auto c : inner_circuits) {
		const auto ct = c->getCircuitType();
		if (ct == CircuitType::MultiCircuit) {
		    put(ct);
		    std::vector<char> data =
			dynamic_cast<MultiCircuit *>(c)
			    ->readSerializedFromImportFile();
		    for (char d : data)
			m_buffer.push_back(d);
		    continue;
		}
		if (ct == CircuitType::Clock) {
		    const unsigned interval =
			dynamic_cast<CircuitClock *>(c)->getInterval();
		    put(ct).put(interval / 256).put(interval % 256);
		    continue;
		}
		if (ct == CircuitType::Multiplex ||
		    ct == CircuitType::Demultiplex) {
		    const auto input_slots =
			(ct == CircuitType::Multiplex)
			    ? (dynamic_cast<Multiplexer *>(c)->getInputSlots())
			    : (dynamic_cast<Demultiplexer *>(c)
				   ->getInputSlots());
		    const auto control_slots = c->getInputN() - input_slots;
		    put(ct).put(input_slots).put(control_slots);
		    continue;
		}
		put(ct).put(c->getInputN()).put(c->getOutputN());
	    }
	}

	// writing data on all the connections
	if (!connections.empty()) {
	    put(BlockType::CONNECT);
	    for (auto &c : connections) {
		CircuitType startType(c->getSrc()->getCircuitType()),
		    endType(c->getDest()->getCircuitType());
		// insert first circuit
		if (startType == CircuitType::In)
		    put(BlockType::IN).put(findPos(inputs, c->getSrc())).put(0);
		else
		    put(BlockType::INNER)
			.put(findPos(inner_circuits, c->getSrc()))
			.put(c->getSrcOutSlot());

		// insert second circuit
		if (endType == CircuitType::Out)
		    put(BlockType::OUT)
			.put(findPos(outputs, c->getDest()))
			.put(0);
		else
		    put(BlockType::INNER)
			.put(findPos(inner_circuits, c->getDest()))
			.put(c->getDestInSlot());
	    }
	}

	end();
    }
};

// helper functions to know how many arguments to put in constructor
auto
getCircuitByArg(CircuitType ct, unsigned in, unsigned out) -> std::unique_ptr<Circuit> {
    switch (ct) {
    case CircuitType::Clock:
	return std::make_unique<CircuitClock>(in * 256 + out);
    case CircuitType::Id: return std::make_unique<CircuitId>();
    case CircuitType::Non: return std::make_unique<CircuitNon>();
    case CircuitType::HalfSubtractor:
	return std::make_unique<CircuitHalfSubtractor>();
    case CircuitType::Subtractor: return std::make_unique<CircuitSubtractor>();
    case CircuitType::HalfAdder: return std::make_unique<CircuitHalfAdder>();
    case CircuitType::Adder: return std::make_unique<CircuitAdder>();
    case CircuitType::Comparator: return std::make_unique<CircuitComparator>();
    case CircuitType::SRLatch: return std::make_unique<CircuitSRLatch>();
    case CircuitType::DLatch: return std::make_unique<CircuitDLatch>();
    case CircuitType::And: return std::make_unique<CircuitAnd>(in);
    case CircuitType::Or: return std::make_unique<CircuitOr>(in);
    case CircuitType::Xor: return std::make_unique<CircuitXor>(in);
    case CircuitType::Nand: return std::make_unique<CircuitNand>(in);
    case CircuitType::Nor: return std::make_unique<CircuitNor>(in);
    case CircuitType::Nxor: return std::make_unique<CircuitNxor>(in);
    case CircuitType::Spliter: return std::make_unique<CircuitSpliter>(out);
    case CircuitType::Multiplex: return std::make_unique<Multiplexer>(in, out);
    case CircuitType::Demultiplex:
	return std::make_unique<Demultiplexer>(in, out);
    case CircuitType::TFlipFlop: return std::make_unique<TFlipFlop>(in, out);
    case CircuitType::DFlipFlop: return std::make_unique<DFlipFlop>(in, out);
    case CircuitType::JKFlipFlop: return std::make_unique<JKFlipFlop>(in, out);
    case CircuitType::SRFlipFlop: return std::make_unique<SRFlipFlop>(in, out);
    case CircuitType::Encoder: return std::make_unique<CircuitEncoder>(in, out);
    case CircuitType::Decoder: return std::make_unique<CircuitDecoder>(in, out);
    default: return nullptr;
    }
}

class SmartReader {
  private:
    unsigned m_data_size;
    unsigned m_pointer;
    char *m_data;
    bool m_has_data;
    void clear() {
	if (m_has_data) {
	    delete[] m_data;
	    m_pointer = m_data_size = m_has_data = false;
	}
    }

  public:
    SmartReader(const std::string &file_name) : m_pointer(0), m_has_data(true) {
	struct stat stat_buf;
	int rc = stat(file_name.c_str(), &stat_buf);
	m_data_size = (rc == 0) ? stat_buf.st_size : 0;
	if (m_data_size) {
	    m_data = new char[m_data_size];
	    std::ifstream file(file_name, std::ios::in | std::ios::binary);
	    file.read(m_data, m_data_size);
	} else
	    m_has_data = false;
    }

    SmartReader(char *data, unsigned len)
	: m_data(data), m_data_size(len), m_pointer(0), m_has_data(true) {}

    ~SmartReader() { clear(); }

    inline auto empty() -> bool { return !m_has_data; }

    auto get(unsigned &val) -> bool {
	if (m_has_data) {
	    auto curr_token = static_cast<TokenType>(m_data[m_pointer++]);
	    val = static_cast<unsigned char>(m_data[m_pointer++]);
	    if (curr_token == TokenType::NUMBER) {
		if (m_pointer >= m_data_size)
		    clear();
		return true;
	    } else
		m_pointer -= 2;
	}
	return false;
    }
    auto get(BlockType &val) -> bool {
	if (m_has_data) {
	    auto curr_token = static_cast<TokenType>(m_data[m_pointer++]);
	    val = static_cast<BlockType>(m_data[m_pointer++]);
	    if (curr_token == TokenType::BLOCK) {
		if (m_pointer >= m_data_size)
		    clear();
		return true;
	    } else
		m_pointer -= 2;
	}
	return false;
    }
    auto get(CircuitType &val) -> bool {
	if (m_has_data) {
	    auto curr_token = static_cast<TokenType>(m_data[m_pointer++]);
	    val = static_cast<CircuitType>(m_data[m_pointer++]);
	    if (curr_token == TokenType::CIRCUIT) {
		if (m_pointer >= m_data_size)
		    clear();
		return true;
	    } else
		m_pointer -= 2;
	}
	return false;
    }

    auto end() -> bool {
	if (m_has_data) {
	    auto curr_token = static_cast<TokenType>(m_data[m_pointer]);
	    auto val = static_cast<unsigned>(m_data[m_pointer + 1]);
	    if (curr_token == TokenType::END && val == 0) {
		clear();
		return true;
	    }
	}
	return false;
    }

    auto getRecursiveMultiCircuit() -> std::unique_ptr<Circuit> {
	std::vector<char> temp;
	unsigned indent_count = 0;
	while (true) {
	    if (m_pointer == m_data_size) {
		clear();
		return nullptr;
	    }
	    auto curr_token = static_cast<TokenType>(m_data[m_pointer++]);
	    auto val = static_cast<unsigned>(m_data[m_pointer++]);
	    temp.push_back(static_cast<char>(curr_token));
	    temp.push_back(static_cast<char>(val));
	    if (curr_token == TokenType::END) {
		if (indent_count == 0)
		    break;
		else
		    indent_count--;
	    }
	    if (curr_token == TokenType::CIRCUIT &&
		static_cast<CircuitType>(val) == CircuitType::MultiCircuit)
		indent_count++;
	}
	unsigned size = temp.size();
	char *parsed_data = new char[size];
	for (unsigned i = 0; i < size; i++)
	    parsed_data[i] = temp[i];
	SmartReader sr(parsed_data, size);
	std::unique_ptr<Circuit> ret(sr.parse());
	if (ret)
	    return ret;
	else
	    return nullptr;
    }

    auto parse() -> MultiCircuit * {
	// getting number of inputs and outputs
	unsigned in, out, temp;
	BlockType frst_token, scnd_token;
	if (!get(frst_token))
	    return nullptr;
	if (frst_token != BlockType::IN && frst_token != BlockType::OUT)
	    return nullptr;

	if (!get(temp))
	    return nullptr;
	if (frst_token == BlockType::IN)
	    in = temp;
	else
	    out = temp;

	if (!get(scnd_token))
	    return nullptr;
	if (scnd_token != BlockType::IN && scnd_token != BlockType::OUT)
	    return nullptr;
	if (frst_token == scnd_token)
	    return nullptr;
	if (!get(temp))
	    return nullptr;
	if (scnd_token == BlockType::IN)
	    in = temp;
	else
	    out = temp;

	TempBox mc = TempBox(new MultiCircuit(in, out));
	BlockType bt;
	CircuitType ct;
	if (!get(bt))
	    return end() ? mc.move() : nullptr;

	// inserting circuits into the MultiCircuit
	if (bt == BlockType::INNER) {
	    while (true) {
		std::unique_ptr<Circuit> temp;
		if (!get(ct))
		    break;
		if (ct != CircuitType::MultiCircuit) {
		    if (!get(in))
			return nullptr;
		    if (!get(out))
			return nullptr;
		    temp = getCircuitByArg(ct, in, out);
		} else {
		    temp = getRecursiveMultiCircuit();
		}
		if (!temp)
		    return nullptr;
		mc.get()->addCircuit(temp);
	    }

	    if (end())
		return mc.move();
	    if (!get(bt))
		return nullptr;
	}
	// inserting circuit connections into MultiCircuit
	if (bt != BlockType::CONNECT)
	    return nullptr;
	while (!end()) {
	    unsigned sigin, sigout;
	    if (!get(frst_token))
		return nullptr;
	    if (frst_token != BlockType::IN && frst_token != BlockType::INNER)
		return nullptr;
	    if (!get(in))
		return nullptr;
	    if (!get(sigin))
		return nullptr;
	    if (!get(scnd_token))
		return nullptr;
	    if (scnd_token != BlockType::OUT && scnd_token != BlockType::INNER)
		return nullptr;
	    if (!get(out))
		return nullptr;
	    if (!get(sigout))
		return nullptr;
	    if (frst_token == BlockType::INNER) {
		if (scnd_token == BlockType::INNER) // INNER and INNER
		    mc.get()->connectInnerCircuits(in, sigin, out, sigout);
		else // INNER and OUT
		    mc.get()->connect2output(in, sigin, out);
	    } else {
		if (scnd_token == BlockType::OUT) // IN and OUT
		    mc.get()->connectinput2output(in, out);
		else // IN and INNER
		    mc.get()->connect2input(out, sigout, in);
	    }
	}

	return mc.move();
    }
};
} // namespace

void circuit::scene2file(QString file_name,
			 const std::vector<CircuitIn *> &inputs,
			 const std::vector<CircuitOut *> &outputs,
			 const std::vector<Circuit *> &inner_circuits,
			 const std::vector<Connection *> &connections) {
    if (!file_name.endsWith(extension))
	file_name.append(extension);

    SmartWriter sw(file_name.toStdString());
    std::vector<Circuit *> in_cast, out_cast;

    for (auto c : inputs)
	in_cast.push_back(static_cast<Circuit *>(c));
    for (auto c : outputs)
	out_cast.push_back(static_cast<Circuit *>(c));

    sw.save(in_cast, out_cast, inner_circuits, connections);
}

auto circuit::file2mc(QString file_name) -> MultiCircuit * {
    if (!file_name.endsWith(extension))
	return nullptr;

    SmartReader sr(file_name.toStdString());
    if (sr.empty())
	return nullptr;

    MultiCircuit *mc = sr.parse();
    if (mc) {
	mc->setName(file_name);
    }
    return mc;
}

/////////////////////////////////////////////////////////
//
// MultiCircuit Implementation
//
/////////////////////////////////////////////////////////

void MultiCircuit::setName(const QString &file_name) {
    QString m_path = file_name;
    if (file_name.isEmpty()) {
	m_name = m_path;
    } else {
	QFileInfo fi(file_name);
	QString fn = fi.fileName();
	auto file_name_list = fn.split(".");
	m_name = file_name_list.at((file_name_list.at(0).isEmpty()) ? 1 : 0);

	m_source_code.clear();
	char *data = nullptr;
	std::string path = m_path.toStdString();
	struct stat stat_buf;
	int rc = stat(path.c_str(), &stat_buf);
	auto data_size = (rc == 0) ? stat_buf.st_size : 0;
	if (data_size) {
	    data = new char[data_size];
	    std::ifstream file(path, std::ios::in | std::ios::binary);
	    file.read(data, data_size);
	    for (auto i = 0; i < data_size; i++) {
		m_source_code.push_back(data[i]);
	    }
	    delete[] data;
	}
    }
}

MultiCircuit::MultiCircuit(unsigned in, unsigned out, QString path)
    : Circuit(CircuitType::MultiCircuit)
    , m_signal_in(in)
    , m_signal_out(out)
    , m_removedCircuitN(0) {
    setName(path);
    for (unsigned i = 0; i < in; i++) {
	m_signal_in[i] = std::make_unique<CircuitId>();
	m_signal_in[i]->setParentItem(this);
	m_signal_in[i]->hide();
    }
    for (unsigned i = 0; i < out; i++) {
	m_signal_out[i] = std::make_unique<CircuitId>();
	m_signal_out[i]->setParentItem(this);
	m_signal_out[i]->hide();
    }
}

void MultiCircuit::connectInput(std::weak_ptr<Signal> sig, unsigned u) {
#ifdef __USE_UTIL__
    if (u >= getInputN())
	util::upozorenje(std::string("MultiCircuit.connectInput: ")
			     .append(std::to_string(u))
			     .append(" was passed but the MultiCircuit has ")
			     .append(std::to_string(getInputN()))
			     .append(" inputs currently"));
    else
#endif
	m_signal_in[u]->connectInput(sig);
}

auto MultiCircuit::getSignal(unsigned u) const -> std::weak_ptr<Signal> {
#ifdef __USE_UTIL__
    if (u >= getOutputN())
	util::greska(std::string("MultiCircuit.getSignal: ")
			 .append(std::to_string(u))
			 .append(" was passed but the MultiCircuit has ")
			 .append(std::to_string(getInputN()))
			 .append(" outputs currently"));
#endif
    return m_signal_out[u]->getSignal();
}

void MultiCircuit::removeInput(unsigned u) {
    unsigned end = getInputN();
#ifdef __USE_UTIL__
    if (u >= end) {
	util::upozorenje(std::string("MultiCircuit.removeInput: has ")
			     .append(std::to_string(end))
			     .append(" inputs but ")
			     .append(std::to_string(u))
			     .append(" was passed"));
	return;
    }
#endif
    for (auto i = u; u < end - 1; i++) {
	m_signal_in[i] = std::move(m_signal_in[i + 1]);
    }
    m_signal_in.pop_back();
}

void MultiCircuit::removeOutput(unsigned u) {
    unsigned end = getOutputN();
#ifdef __USE_UTIL__
    if (u >= end) {
	util::upozorenje(std::string("MultiCircuit.removeInput: has ")
			     .append(std::to_string(end))
			     .append(" outputs but ")
			     .append(std::to_string(u))
			     .append(" was passed"));
	return;
    }
#endif
    for (auto i = u; u < end - 1; i++) {
	m_signal_out[i] = std::move(m_signal_out[i + 1]);
    }
    m_signal_out.pop_back();
}

inline void MultiCircuit::addInput() {
    unsigned pos = getInputN();
    m_signal_in.push_back(std::make_unique<CircuitId>());
    m_signal_in[pos]->setParentItem(this);
    m_signal_in[pos]->hide();
}

inline void MultiCircuit::addOutput() {
    unsigned pos = getOutputN();
    m_signal_out.push_back(std::make_unique<CircuitId>());
    m_signal_out[pos]->setParentItem(this);
    m_signal_out[pos]->hide();
}

auto MultiCircuit::addCircuit(std::unique_ptr<Circuit> &c) -> unsigned {
    unsigned i = 0;
    if (m_removedCircuitN) {
	while (m_inner_circuits[i++])
	    ;
	m_inner_circuits[i] = std::move(c);
	m_removedCircuitN--;
    } else {
	i = getInnerN();
	m_inner_circuits.push_back(std::move(c));
    }

    m_inner_circuits[i]->setParentItem(this);
    m_inner_circuits[i]->hide();
    return i;
}

void MultiCircuit::removeCircuit(unsigned u) {
#ifdef __USE_UTIL__
    if (u >= getInnerN())
	util::upozorenje(std::string("MultiCircuit.removeCircuit: has ")
			     .append(std::to_string(getInnerN()))
			     .append(" circuits but ")
			     .append(std::to_string(u))
			     .append(" was passed"));
    else
#endif
    {
	m_inner_circuits[u] = nullptr;
	m_removedCircuitN++;
    }
}

void MultiCircuit::connectinput2output(unsigned input, unsigned output) {
#ifdef __USE_UTIL__
    if (input >= getInputN()) {
	util::upozorenje(std::string("MultiCircuit.connectinput2output: has ")
			     .append(std::to_string(getInputN()))
			     .append(" inputs but ")
			     .append(std::to_string(input))
			     .append(" was passed"));
	return;
    }

    if (output >= getOutputN()) {
	util::upozorenje(std::string("MultiCircuit.connectinput2output: has ")
			     .append(std::to_string(getOutputN()))
			     .append(" outputs but ")
			     .append(std::to_string(output))
			     .append(" was passed"));
	return;
    }
#endif
    circuit::connect(
	m_signal_in[input].get(), 0, m_signal_out[output].get(), 0);
}

void MultiCircuit::connect2input(unsigned circuitId,
				 unsigned circuitIn,
				 unsigned input) {
#ifdef __USE_UTIL__
    if (input >= getInputN()) {
	util::upozorenje(std::string("MultiCircuit.connect2input: has ")
			     .append(std::to_string(getInputN()))
			     .append(" inputs but ")
			     .append(std::to_string(input))
			     .append(" was passed"));
	return;
    }

    if (circuitId > getInnerN()) {
	util::upozorenje(std::string("MultiCircuit.connect2input: has ")
			     .append(std::to_string(getInnerN()))
			     .append(" circuits but ")
			     .append(std::to_string(circuitId))
			     .append(" was passed"));
	return;
    }

    if (!m_inner_circuits[circuitId]) {
	util::upozorenje(std::string("MultiCircuit.connect2input: circuit ID ")
			     .append(std::to_string(circuitId))
			     .append(" is not active"));
	return;
    }
#endif

    circuit::connect(m_signal_in[input].get(),
		     0,
		     m_inner_circuits[circuitId].get(),
		     circuitIn);
}

void MultiCircuit::connect2output(unsigned circuitId,
				  unsigned circuitOut,
				  unsigned output) {
#ifdef __USE_UTIL__
    if (output >= getOutputN()) {
	util::upozorenje(std::string("MultiCircuit.connect2output: has ")
			     .append(std::to_string(getOutputN()))
			     .append(" outputs but ")
			     .append(std::to_string(output))
			     .append(" was passed"));
	return;
    }

    if (circuitId > getInnerN()) {
	util::upozorenje(std::string("MultiCircuit.connect2output: has ")
			     .append(std::to_string(getInnerN()))
			     .append(" circuits but ")
			     .append(std::to_string(circuitId))
			     .append(" was passed"));
	return;
    }

    if (!m_inner_circuits[circuitId]) {
	util::upozorenje(std::string("MultiCircuit.connect2input: circuit ID ")
			     .append(std::to_string(circuitId))
			     .append(" is not active"));
	return;
    }
#endif
    circuit::connect(m_inner_circuits[circuitId].get(),
		     circuitOut,
		     m_signal_out[output].get(),
		     0);
}

void MultiCircuit::connectInnerCircuits(unsigned circuitIdIn,
					unsigned input,
					unsigned circuitIdOut,
					unsigned output) {
#ifdef __USE_UTIL__
    if (circuitIdIn > getInnerN()) {
	util::upozorenje(std::string("MultiCircuit.connectInnerCircuits: has ")
			     .append(std::to_string(getInnerN()))
			     .append(" circuits but ")
			     .append(std::to_string(circuitIdIn))
			     .append(" was passed"));
	return;
    }

    if (!m_inner_circuits[circuitIdIn]) {
	util::upozorenje(
	    std::string("MultiCircuit.connectInnerCircuits: circuit ID ")
		.append(std::to_string(circuitIdIn))
		.append(" is not active"));
	return;
    }

    if (circuitIdOut > getInnerN()) {
	util::upozorenje(std::string("MultiCircuit.connectInnerCircuits: has ")
			     .append(std::to_string(getInnerN()))
			     .append(" circuits but ")
			     .append(std::to_string(circuitIdOut))
			     .append(" was passed"));
	return;
    }

    if (!m_inner_circuits[circuitIdOut]) {
	util::upozorenje(
	    std::string("MultiCircuit.connectInnerCircuits: circuit ID ")
		.append(std::to_string(circuitIdOut))
		.append(" is not active"));
	return;
    }
#endif
    circuit::connect(m_inner_circuits[circuitIdIn].get(),
		     input,
		     m_inner_circuits[circuitIdOut].get(),
		     output);
}
