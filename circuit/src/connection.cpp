#include "connection.hpp"
#include <QPainter>
#include <QPen>
#include <QPropertyAnimation>
#include <circuitutil.hpp>

Connection::Connection(Circuit *circuitsrc,
		       Circuit *circuitdest,
		       unsigned srcslot,
		       unsigned destslot,
		       QGraphicsItem *parent)
    : QGraphicsPathItem(parent)
    , m_srcCircuit(circuitsrc)
    , m_destCircuit(circuitdest)
    , m_srcOutSlot(srcslot)
    , m_destInSlot(destslot) {

    circuit::connect(m_srcCircuit, m_srcOutSlot, m_destCircuit, m_destInSlot);
    QObject::connect(
	m_srcCircuit, &Circuit::being_deleted, this, &Connection::destroy);
    QObject::connect(
	m_destCircuit, &Circuit::being_deleted, this, &Connection::destroy);
    QObject::connect(m_srcCircuit->getSignal(m_srcOutSlot).lock().get(),
		     &Signal::changedValue,
		     this,
		     &Connection::repaint);
    changePath();
}

auto Connection::getSrcOutPoint() -> QPointF {
    return mapToScene(m_srcCircuit->getOutPutPositions()[m_srcOutSlot]) +
	   m_srcCircuit->scenePos();
}

auto Connection::getDestInPoint() -> QPointF {
    return mapToScene(m_destCircuit->getInputPositions()[m_destInSlot]) +
	   m_destCircuit->scenePos();
}

void Connection::changePath() {
    auto srcPosition = getSrcOutPoint();
    auto destPosition = getDestInPoint();
    QPainterPath path;
    path.moveTo(srcPosition);

    // line breaks on this to points
    path.lineTo((destPosition.rx() + srcPosition.rx()) / 2, srcPosition.ry());
    path.lineTo((destPosition.rx() + srcPosition.rx()) / 2, destPosition.ry());

    path.lineTo(destPosition.rx(), destPosition.ry());
    setPath(path);
}

void Connection::paint(QPainter *painter,
		       const QStyleOptionGraphicsItem *,
		       QWidget *) {
    QPen m_pen;
    changePath();
    SignalType sig = m_srcCircuit->getSignal(m_srcOutSlot).lock()->value();

    if (sig == SignalType::True) {
	m_pen.setColor(Qt::green);
    }

    if (sig == SignalType::False) {
	m_pen.setColor(Qt::red);
    }

    if (sig == SignalType::Not_Connected) {
	m_pen.setColor(Qt::gray);
    }
    painter->setPen(m_pen);
    painter->drawPath(path());
}

auto Connection::hasSlot(Circuit *c, QPointF point, bool option) -> unsigned {
    std::vector<QPointF> points = (option == __HAS_INPUT__)
				      ? c->getInputPositions()
				      : c->getOutPutPositions();
    unsigned i = 0;
    for (auto &iter : points) {
	const auto realPositionX = iter.rx() + c->scenePos().rx();
	const auto realPositionY = iter.ry() + c->scenePos().ry();
	if (realPositionX == point.rx() && realPositionY == point.ry()) {
	    return i;
	}
	i += 1;
    }

    return i;
}
