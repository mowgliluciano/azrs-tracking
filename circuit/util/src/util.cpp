#include <cstdlib>
#include <iostream>
#include <util.hpp>

void util::greska(std::string s) {
    std::cerr << "ERROR: " << s << "!" << std::endl;
    std::exit(EXIT_FAILURE);
}

void util::upozorenje(std::string s) {
    std::cerr << "WARNING: " << s << "!" << std::endl;
}
