#ifndef __UTIL_HPP__
#define __UTIL_HPP__

#include <string>

namespace util {
// Stops program execution
void greska(std::string s);

// Doesn't stop program execution
void upozorenje(std::string s);
} // namespace util

#endif // UTIL_HPP
