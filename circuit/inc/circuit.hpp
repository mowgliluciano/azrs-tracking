﻿#ifndef CIRCUIT_HPP
#define CIRCUIT_HPP

#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QObject>
#include <QPen>
#include <QTimer>
#include <enumeration.hpp>
#include <memory>
#include <signal.hpp>
#include <vector>

//  --Base class
// When inherited, should be constructed with : Circuit(CircuitType)
class Circuit : public QObject, public QGraphicsRectItem {
    Q_OBJECT

  private:
    const CircuitType m_ctype;
    Circuit(Circuit &) = delete;
    Circuit &operator=(const Circuit &) = delete;
    RotationType m_circuitRotation;

  protected:
    Circuit(CircuitType c, QGraphicsItem *parent = nullptr);
    QPen m_pen;
    QPointF m_offset;
    std::vector<QPointF> m_inputPostions;
    std::vector<QPointF> m_outputPostions;
    QColor SignalType2Color(SignalType) const;
  public slots:
    // When signal changeValue is emited from clas Signal, this function is used
    // to re-evaluate the output values of the circuit
    void re_evaluate(void);
  signals:
    void being_deleted(void);

  public:
    inline CircuitType getCircuitType(void) const { return m_ctype; }
    virtual ~Circuit() { emit being_deleted(); }

    // Handled and implemented by the INPUT interfaces:

    // Returns the number of input signals
    virtual unsigned getInputN(void) const = 0;

    // Attaches a signal to an input slot
    virtual void connectInput(std::weak_ptr<Signal>, unsigned) = 0;

    // Handled and implemented by the OUTPUT interfaces:

    // Returns the number of output signals
    virtual unsigned getOutputN(void) const = 0;

    // Returns one of the output signals of the circuit
    virtual std::weak_ptr<Signal> getSignal(unsigned) const = 0;

    // Must be written by the user:
    virtual void evaluate(void) = 0;

    const std::vector<QPointF> getInputPositions() const;
    const std::vector<QPointF> getOutPutPositions() const;

    inline void setRotationType(RotationType r) { m_circuitRotation = r; }

    // QGraphicsItem interface
  protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
};

//  --Input-Output handling presets

class JustInputCircuit : public Circuit {
  protected:
    SingleInput m_sig;

  private:
    std::weak_ptr<Signal> getSignal(unsigned) const final;

  public:
    JustInputCircuit(CircuitType c) : Circuit(c) {}
    virtual ~JustInputCircuit() {
	//        std::cout << "[~JustInputCircuit]" << std::endl;
    }
    inline unsigned getInputN(void) const final { return 1; }
    void connectInput(std::weak_ptr<Signal>, unsigned = 0) final;
    inline unsigned getOutputN(void) const final { return 0; }
};

class JustOutputCircuit : public Circuit {
  protected:
    SingleOutput m_sig;

  private:
    void connectInput(std::weak_ptr<Signal>, unsigned) final;

  public:
    JustOutputCircuit(CircuitType c);
    virtual ~JustOutputCircuit() {
	//        std::cout << "[~JustOutputCircuit]" << std::endl;
    }
    inline unsigned getInputN(void) const final { return 0; }
    inline unsigned getOutputN(void) const final { return 1; }
    std::weak_ptr<Signal> getSignal(unsigned u = 0) const final;
};

class SingleInputOutputCircuit : public Circuit {
  protected:
    SingleInput m_sig_in;
    SingleOutput m_sig_out;

  public:
    SingleInputOutputCircuit(CircuitType c);
    virtual ~SingleInputOutputCircuit() {
	//        std::cout << "[~SingleInputOutputCircuit]" << std::endl;
    }
    inline unsigned getInputN(void) const final { return 1; }
    inline unsigned getOutputN(void) const final { return 1; }
    void connectInput(std::weak_ptr<Signal>, unsigned = 0) final;
    std::weak_ptr<Signal> getSignal(unsigned u = 0) const final;
};

class MultiInputCircuit : public Circuit {
  protected:
    MultiInput m_sig_in;
    SingleOutput m_sig_out;
    void setConCoords();

  public:
    MultiInputCircuit(CircuitType c, unsigned u);
    virtual ~MultiInputCircuit() {
	//        std::cout << "[~MultiInputCircuit]" << std::endl;
    }
    inline unsigned getInputN(void) const final { return m_sig_in.getInputN(); }
    inline unsigned getOutputN(void) const final { return 1; }
    void connectInput(std::weak_ptr<Signal>, unsigned) final;
    std::weak_ptr<Signal> getSignal(unsigned u = 0) const final;
};

class MultiOutputCircuit : public Circuit {
  protected:
    SingleInput m_sig_in;
    MultiOutput m_sig_out;

  public:
    MultiOutputCircuit(CircuitType c, unsigned u);
    virtual ~MultiOutputCircuit() {
	//        std::cout << "[~MultiOutputCircuit]" << std::endl;
    }
    inline unsigned getInputN(void) const final { return 1; }
    inline unsigned getOutputN(void) const final {
	return m_sig_out.getOutputN();
    }
    void connectInput(std::weak_ptr<Signal>, unsigned = 0) final;
    std::weak_ptr<Signal> getSignal(unsigned) const final;
};

class MultiInputOutputCircuit : public Circuit {
  protected:
    MultiInput m_sig_in;
    MultiOutput m_sig_out;

  public:
    MultiInputOutputCircuit(CircuitType c, unsigned u_in, unsigned u_out);
    virtual ~MultiInputOutputCircuit() {
	//        std::cout << "[~MultiInputOutputCircuit]" << std::endl;
    }
    inline unsigned getInputN(void) const final { return m_sig_in.getInputN(); }
    inline unsigned getOutputN(void) const final {
	return m_sig_out.getOutputN();
    }
    void connectInput(std::weak_ptr<Signal>, unsigned = 0) final;
    std::weak_ptr<Signal> getSignal(unsigned) const final;
};

//  --Different Circuits implementations

// CircuitType::Id
class CircuitId : public SingleInputOutputCircuit {

  public:
    CircuitId();
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// CircuitType::And
class CircuitAnd : public MultiInputCircuit {
  public:
    CircuitAnd(unsigned i = 2) : MultiInputCircuit(CircuitType::And, i) {}
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// CircuitType::Or
class CircuitOr : public MultiInputCircuit {
  public:
    CircuitOr(unsigned i = 2);
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// CircuitType::Xor
class CircuitXor : public MultiInputCircuit {
  public:
    CircuitXor(unsigned i = 2);
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// CircuitType::Non
class CircuitNon : public SingleInputOutputCircuit {
  public:
    CircuitNon(void);
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// CircuitType::Nand
class CircuitNand : public MultiInputCircuit {
  public:
    CircuitNand(unsigned num_of_inputs = 2);
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// CircuitType::Nor
class CircuitNor : public MultiInputCircuit {
  public:
    CircuitNor(unsigned num_of_inputs = 2);
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// CircuitType::Nxor
class CircuitNxor : public MultiInputCircuit {
  public:
    CircuitNxor(unsigned i = 2);
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// CircuitType::Multiplexer
class Multiplexer : public MultiInputCircuit {
  public:
    Multiplexer(unsigned inputSlots = 4, unsigned controlSlots = 2);
    inline unsigned getInputSlots(void) const { return m_inputSlots; }
    inline unsigned getControlSlots(void) const { return m_controlSlots; }
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;

  private:
    const unsigned m_inputSlots;
    const unsigned m_controlSlots;
    void SetInputPositions();
    void SetOutputPositions();
};

// CircuitType::Demultiplex
class Demultiplexer : public MultiInputOutputCircuit {
  public:
    Demultiplexer(unsigned inputSlots = 1, unsigned controlSlots = 2);
    inline unsigned getInputSlots(void) const { return m_inputSlots; }
    inline unsigned getControlSlots(void) const { return m_controlSlots; }
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;

  private:
    const unsigned m_inputSlots;
    const unsigned m_controlSlots;
    void SetInputPositions();
    void SetOutputPositions();
};

// CircuitType::Coder
class CircuitEncoder : public MultiInputOutputCircuit {
  public:
    CircuitEncoder(unsigned num_of_inputs = 4, unsigned num_of_outputs = 2);
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;

  private:
    void SetInputPositions();
    void SetOutputPositions();
};

class CircuitDecoder : public MultiInputOutputCircuit {
  public:
    CircuitDecoder(unsigned num_of_inputs = 2, unsigned num_of_outputs = 4);
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;

  private:
    void SetInputPositions();
    void SetOutputPositions();
};

class JKFlipFlop : public MultiInputOutputCircuit {
  public:
    JKFlipFlop(unsigned num_of_inputs = 3, unsigned num_of_outputs = 2);
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

class DFlipFlop : public MultiInputOutputCircuit {
  public:
    DFlipFlop(unsigned num_of_inputs = 2, unsigned num_of_outputs = 2);
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

class TFlipFlop : public MultiInputOutputCircuit {
  public:
    TFlipFlop(unsigned num_of_inputs = 2, unsigned num_of_outputs = 2);
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

class SRFlipFlop : public MultiInputOutputCircuit {
  public:
    SRFlipFlop(unsigned num_of_inputs = 3, unsigned num_of_outputs = 2);
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// polusabirac
class CircuitHalfAdder : public MultiInputOutputCircuit {
  public:
    CircuitHalfAdder();
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// potpuni sabirac
class CircuitAdder : public MultiInputOutputCircuit {
  public:
    CircuitAdder();
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

class CircuitHalfSubtractor : public MultiInputOutputCircuit {
  public:
    CircuitHalfSubtractor();
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

class CircuitSubtractor : public MultiInputOutputCircuit {
  public:
    CircuitSubtractor();
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// clock circuit
class CircuitClock : public JustOutputCircuit {
  public:
    CircuitClock(unsigned t = 1000);
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
    inline unsigned getInterval(void) const { return m_interval; }
    inline void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *) override;

  private:
    unsigned m_tb;
    unsigned m_ts;
    QPointF getBig() const;
    QPointF getSmall() const;
    const unsigned m_interval;
    bool m_paused;
    QTimer *m_timer;
};

// spliter class
class CircuitSpliter : public MultiOutputCircuit {
  public:
    CircuitSpliter(unsigned num_of_outputs = 3);

    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
    std::vector<SignalType> getAllSignals() const;
};

// comparator class
class CircuitComparator : public MultiInputOutputCircuit {
  public:
    CircuitComparator();
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// SR reza
class CircuitSRLatch : public MultiInputOutputCircuit {
  public:
    CircuitSRLatch();
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// D reza
class CircuitDLatch : public MultiInputOutputCircuit {
  public:
    CircuitDLatch();
    void evaluate(void) override;
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
};

// Ulazno kolo koje emituje signal
class CircuitIn : public JustOutputCircuit {
  public:
    CircuitIn(bool s = true);
    ~CircuitIn() {
	//        std::cout << "[~CircuitIn]" << std::endl;
    }
    void evaluate() override {
	m_sig.put(m_sig.getSignal().lock()->negateValue());
    }
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *) override {
	re_evaluate();
    }
};

// Izlaz (lampica)

class CircuitOut : public JustInputCircuit {
  public:
    CircuitOut();
    ~CircuitOut() {
	//        std::cout << "[~CircuitOut]" << std::endl;
    }
    inline void evaluate(void) override { m_signal = m_sig.value(); }
    inline SignalType value(void) const { return m_signal; }
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;

  private:
    SignalType m_signal;
};

// CircuitType::MultiCircuit
class MultiCircuit : public Circuit {
  private:
    std::vector<std::unique_ptr<CircuitId>> m_signal_in;
    std::vector<std::unique_ptr<CircuitId>> m_signal_out;
    std::vector<std::unique_ptr<Circuit>> m_inner_circuits;
    std::vector<char> m_source_code;
    unsigned m_removedCircuitN;
    QString m_name;
    void evaluate(void) override {}

  public:
    MultiCircuit(unsigned in = 0, unsigned out = 0, QString path = "");
    ~MultiCircuit() {
	//        std::cout << "[~MultiCircuit]" << std::endl;
    }
    inline unsigned getInputN(void) const override {
	return m_signal_in.size();
    }
    inline unsigned getOutputN(void) const override {
	return m_signal_out.size();
    }
    inline unsigned getInnerN(void) const { return m_inner_circuits.size(); }
    void connectInput(std::weak_ptr<Signal>, unsigned) override;
    std::weak_ptr<Signal> getSignal(unsigned) const override;
    void removeInput(unsigned);
    void removeOutput(unsigned);
    void addInput(void);
    void addOutput(void);
    unsigned addCircuit(std::unique_ptr<Circuit> &);
    void removeCircuit(unsigned);
    void connectinput2output(unsigned input, unsigned output);
    void connect2input(unsigned circuitId, unsigned circuitIn, unsigned input);
    void
    connect2output(unsigned circuitId, unsigned circuitOut, unsigned output);
    void connectInnerCircuits(unsigned circuitIdIn,
			      unsigned input,
			      unsigned circuitIdOut,
			      unsigned output);
    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;
    void setName(const QString &name);

    inline std::vector<char> &readSerializedFromImportFile() {
	return m_source_code;
    }
};

#endif // CIRCUIT_HPP
