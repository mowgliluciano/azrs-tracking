#ifndef CIRCUITCLASS_HPP
#define CIRCUITCLASS_HPP

#include <circuit.hpp>
#include <connection.hpp>

namespace circuit {
// function that connect 2 circuits and sets the event listener
//!!!
// SHOULD BE USED TO CONNECT 2 CIRCUITS
//!!!
void connect(Circuit &in, unsigned in_n, Circuit &out, unsigned out_n);
void connect(Circuit *in, unsigned in_n, Circuit *out, unsigned out_n);
void connect(std::shared_ptr<Circuit> &in,
	     unsigned in_n,
	     std::shared_ptr<Circuit> &out,
	     unsigned out_n);
void connect(std::unique_ptr<Circuit> &in,
	     unsigned in_n,
	     std::unique_ptr<Circuit> &out,
	     unsigned out_n);
void disconnect(Circuit &c, unsigned c_n);
void disconnect(Circuit *c, unsigned c_n);
void disconnect(std::shared_ptr<Circuit> &c, unsigned c_n);
void disconnect(std::unique_ptr<Circuit> &c, unsigned c_n);

void scene2file(QString file_name,
		const std::vector<CircuitIn *> &inputs,
		const std::vector<CircuitOut *> &outputs,
		const std::vector<Circuit *> &inner_circuits,
		const std::vector<Connection *> &connections);

MultiCircuit *file2mc(QString file_name);

Circuit *getElementByType(CircuitType t, int numOfInput = 2);
} // namespace circuit

#endif // CIRCUITCLASS_HPP
