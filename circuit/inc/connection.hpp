#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include <circuit.hpp>

#define __HAS_INPUT__ true
#define __HAS_OUTPUT__ false

class Connection : public QObject, public QGraphicsPathItem {
    Q_OBJECT
  private:
    Circuit *m_srcCircuit;
    Circuit *m_destCircuit;
    unsigned m_srcOutSlot;
    unsigned m_destInSlot;
    //   QPen m_pen;
    QPointF getSrcOutPoint();
    QPointF getDestInPoint();
    void changePath();

  public:
    Connection(Circuit *circuitsrc,
	       Circuit *circuitdest,
	       unsigned srcslot,
	       unsigned destslot,
	       QGraphicsItem *parent = nullptr);

    void paint(QPainter *painter,
	       const QStyleOptionGraphicsItem *,
	       QWidget *) override;

    static unsigned hasSlot(Circuit *, QPointF, bool);
    inline Circuit *getSrc() const { return m_srcCircuit; }
    inline Circuit *getDest() const { return m_destCircuit; }
    inline unsigned getSrcOutSlot() const { return m_srcOutSlot; }
    inline unsigned getDestInSlot() const { return m_destInSlot; }
    inline bool operator==(const Connection &scnd) {
	return getSrc() == scnd.getSrc() && getDest() == scnd.getDest() &&
	       getSrcOutSlot() == scnd.getSrcOutSlot() &&
	       getDestInSlot() == scnd.getDestInSlot();
    }
    //    QRectF boundingRect() const;
  public slots:
    void destroy() { delete this; }
    void repaint() { update(); }
};

#endif // CONNECTION_HPP
