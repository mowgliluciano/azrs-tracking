#ifndef SIGNAL_HPP
#define SIGNAL_HPP

#include "enumeration.hpp"
#include <QObject>
#include <memory>
#include <vector>

class Signal : public QObject {
    Q_OBJECT

  private:
    SignalType m_val;
    Signal(Signal &) = delete;

  public:
    Signal(SignalType s = SignalType::Not_Connected) : m_val(s) {}
    ~Signal();
    inline SignalType value(void) const { return m_val; }
    SignalType negateValue(void) const;
    void put(SignalType s);
  signals:
    void changedValue(void);
};
// napravim klasu koju nasleduju ove dve dole
// definisem staticku prom first selected

class SingleInput {
  private:
    std::weak_ptr<Signal> m_sig;

  public:
    SingleInput();
    SignalType value() const;
    inline void putSignal(std::weak_ptr<Signal> sig) { m_sig = sig; }
    inline std::weak_ptr<Signal> getSignal(void) const { return m_sig; }
};

class SingleOutput {
  private:
    std::shared_ptr<Signal> m_sig;

  public:
    SingleOutput();
    inline std::weak_ptr<Signal> getSignal() const { return m_sig; }
    inline void put(SignalType s) { m_sig->put(s); }
    inline SignalType value(void) const { return m_sig->value(); }
};

class MultiInput {
  private:
    std::vector<SingleInput> m_sig;
    const unsigned m_input_n;

  public:
    MultiInput(unsigned u) : m_sig(u), m_input_n(u) {}
    inline unsigned getInputN() const { return m_input_n; }
    void putSignal(std::weak_ptr<Signal>, unsigned);
    bool
    hasSingalType(SignalType s, unsigned begin = 0, unsigned end = 0) const;
    SignalType prioritySignalSearcher(SignalType s1,
				      SignalType s2,
				      SignalType s3,
				      unsigned begin = 0,
				      unsigned end = 0) const;
    bool hasAllEqualSignalType(unsigned begin = 0, unsigned end = 0) const;
    SignalType checkInputSignalValue(unsigned) const;
    std::weak_ptr<Signal> getSignal(unsigned);
    SingleInput &operator[](unsigned);
};

class MultiOutput {
  private:
    std::vector<SingleOutput> m_sig;
    const unsigned m_output_n;

  public:
    MultiOutput(unsigned u) : m_sig(u), m_output_n(u) {}
    inline unsigned getOutputN() const { return m_output_n; }
    void putAll(SignalType s, unsigned begin = 0, unsigned end = 0);
    SignalType value(unsigned) const;
    void put(SignalType, unsigned);
    std::weak_ptr<Signal> getSignal(unsigned u) const;
};

#endif // SIGNAL_HPP
