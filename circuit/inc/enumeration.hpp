#ifndef ENUMERATION_HPP
#define ENUMERATION_HPP

enum class SignalType { True, False, Not_Connected };

enum class CircuitType {
    In = 0,
    Out,
    Id,
    And,
    Or,
    Xor,
    Non,
    Multiplex,
    Demultiplex,
    TFlipFlop,
    DFlipFlop,
    JKFlipFlop,
    SRFlipFlop,
    Encoder,
    Decoder,
    Nand,
    Nor,
    Nxor,
    HalfAdder,
    Adder,
    HalfSubtractor,
    Clock,
    Spliter,
    Subtractor,
    Comparator,
    MultiCircuit,
    SRLatch,
    DLatch
};

enum RotationType { Right = 0, Down = 90, Left = 180, Up = 270 };
#endif // ENUMERATION_HPP
