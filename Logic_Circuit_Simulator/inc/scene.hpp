#ifndef __SCENE__
#define __SCENE__

#include <QGraphicsScene>
#include <circuitutil.hpp>

class QTimer;

enum ActionType { Move, Delete, Connect, Import, None };

class Scene : public QGraphicsScene {
    Q_OBJECT

  public:
    explicit Scene(QObject *parent = nullptr);

    void setCircuitType(CircuitType t);
    void setActionType(ActionType t);
    void deleteSelectedItems();
    void osvezi();
    void rotateItems(RotationType angle);
    void setNumberOfInput(int num);
    // Import/Export
    void setImportFilename(const QString &filename);
    void exportSceneToFile(const QString &filename) const;

  signals:
    void resetActions();

  private:
    // bool checkIfRectangleIsClicked(QGraphicsSceneEvent *e);
    void insertCircuit(QGraphicsSceneMouseEvent *e, CircuitType t);
    QPointF getConnectionPoint(Circuit *circuit, QPointF mousePosition) const;

  private:
    Circuit *_elem;
    CircuitType _circuitType;
    bool _buttonIsSelected = false;
    bool _secondButtonPressed = false;
    ActionType _actionType = ActionType::None;
    QTimer *_timer;
    int _numberOfInput = -1;
    // Connection
    unsigned m_srcPosition;
    unsigned m_destPosition;
    Circuit *m_srcCircuit;
    Circuit *m_destCircuit;
    QString m_importFileName;

    // QGraphicsScene interface
  protected:
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
};

#endif // SCENE
