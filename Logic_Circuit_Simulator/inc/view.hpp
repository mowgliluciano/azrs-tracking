#ifndef __VIEW__
#define __VIEW__

#include <QGraphicsView>
#include <QObject>

class Graphics_view_zoom : public QObject {
    Q_OBJECT
  public:
    Graphics_view_zoom(QGraphicsView *view);
    void gentle_zoom(double factor);
    void center_zoom(double factor);
    void set_modifiers(Qt::KeyboardModifiers modifiers);
    void set_zoom_factor_base(double value);

  private:
    QGraphicsView *m_view;
    double m_zoom_factor_base, m_curr_zoom, m_min_zoom, m_max_zoom;
    QPointF m_target_scene_pos, m_target_viewport_pos;
    bool eventFilter(QObject *object, QEvent *event);
    double calculate_factor(double factor);
  signals:
    void zoomed();
};

#endif // VIEW
