#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "enumeration.hpp"
#include <QMainWindow>

// forward declarations
class Graphics_view_zoom;
class Scene;
class QActionGroup;

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

enum InputType { Gates, Plexers };

class MainWindow : public QMainWindow {
    Q_OBJECT

  public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

  private:
    void onClickButton(CircuitType);
    void setupConnections();
    void setupActions();
    void setupScene();
    void setupView();
    void setInputProperties(CircuitType t);

  private:
    Ui::MainWindow *ui;
    Scene *_scene;
    QActionGroup *_toolBarActions;
    bool _buttonChecked = false;
    InputType _inputType;
    Graphics_view_zoom *_gvz;

  private slots:
    void onActionImport();
    void onActionExport();
    void onActionMove();
    void onActionDelete();
    void onActionConnect();
    void resetActions();
    void onActionInput();
    void onActionZoomIn();
    void onActionZoomOut();
    // void onActionMinimize();
    // void onActionMaximize();
    void onActionQuit();
    void onActionNew();
    void onActionRotateLeft();
    void onActionRotateRight();
};
#endif // MAINWINDOW_H
