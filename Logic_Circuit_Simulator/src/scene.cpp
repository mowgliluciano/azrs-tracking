#include "scene.hpp"
#include <QGraphicsSceneEvent>
#include <QKeyEvent>
#include <QList>
#include <QTimer>
#include <circuitutil.hpp>
#include <cmath>

// ctor
Scene::Scene(QObject *parent) : QGraphicsScene(parent) {
    // tajmer za scenu
    _timer = new QTimer(this);
    connect(_timer, SIGNAL(timeout()), this, SLOT(advance()));
    _timer->start(10);
}

void Scene::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    if (_buttonIsSelected &&
	(_actionType == ActionType::None || _actionType == ActionType::Move)) {
	insertCircuit(event, _circuitType);
	_buttonIsSelected = false;
    }

    if (_actionType == ActionType::Connect) {

	if (!_secondButtonPressed) {

	    // TODO:Refaktorise u funkciju
	    auto obj = QGraphicsScene::itemAt(event->scenePos(), QTransform());
	    if (obj == nullptr) {
		return;
	    }
	    //            m_srcCircuit = static_cast<Circuit*>(obj);
	    m_srcCircuit = qgraphicsitem_cast<Circuit *>(obj);
	    if (m_srcCircuit) {
		QPointF mousePosition = event->scenePos();
		auto poss = getConnectionPoint(m_srcCircuit, mousePosition);

		m_srcPosition =
		    Connection::hasSlot(m_srcCircuit, poss, __HAS_OUTPUT__);
		if (m_srcPosition == m_srcCircuit->getOutputN()) {
		    return;
		}
	    }
	    // provera da li je klikunta tacka
	}
	if (_secondButtonPressed) {
	    auto obj = QGraphicsScene::itemAt(event->scenePos(), QTransform());
	    if (obj == nullptr) {
		return;
	    }

	    m_destCircuit = qgraphicsitem_cast<Circuit *>(obj);
	    if (m_srcCircuit && m_destCircuit) {
		QPointF mousePosition = event->scenePos();
		auto poss = getConnectionPoint(m_destCircuit, mousePosition);

		m_destPosition =
		    Connection::hasSlot(m_destCircuit, poss, __HAS_INPUT__);

		if (m_destPosition == m_destCircuit->getInputN()) {
		    return;
		}
		// provera da li je klikunta tacka
		// if(m_srcPosition == m_destCircuit->scenePos()){
		//    return;
		//}

		auto connection = new Connection(
		    m_srcCircuit, m_destCircuit, m_srcPosition, m_destPosition);
		addItem(connection);
		_buttonIsSelected = false;
	    }
	}
    }

    if (_actionType == ActionType::Import && m_importFileName != "") {
	auto mc = circuit::file2mc(m_importFileName);
	m_importFileName = "";
	if (mc) {
	    addItem(mc);
	    mc->setPos(event->scenePos());
	}
    }

    if (_secondButtonPressed && _actionType == ActionType::Connect) {
	_actionType = None;
	_secondButtonPressed = false;
	emit resetActions();
    } else if (!_secondButtonPressed && _actionType == ActionType::Connect)
	_secondButtonPressed = true;
    else {
	_actionType = None;
	emit resetActions();
    }

    QGraphicsScene::mousePressEvent(event);
}

// brisanje selektovanog kola pritiskom nekog tastera
void Scene::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_Delete) {
	deleteSelectedItems();
    }

    // C as clear
    if (_actionType == ActionType::None && event->key() == Qt::Key_C)
	clear();

    // strelice nam pomeraju scenu gore/dole/levo/desno
    //          W (gore)
    // (levo) A   D (desno)
    //          S (dole)

    // pocetni polozaj nam je left(kao sto je nacrtano na slicicama)

    // gore-W
    if (event->key() == Qt::Key_W) {
	rotateItems(RotationType::Up);
    }

    // desno-A
    if (event->key() == Qt::Key_A) {
	rotateItems(RotationType::Left);
    }

    // dole-S
    if (event->key() == Qt::Key_S) {
	rotateItems(RotationType::Down);
    }

    // levo-D
    if (event->key() == Qt::Key_D) {
	rotateItems(RotationType::Right);
    }

    emit resetActions();
    QGraphicsScene::keyPressEvent(event);
}

void Scene::setCircuitType(CircuitType t) {
    _circuitType = t;
    _buttonIsSelected = true;
}

void Scene::setActionType(ActionType t) { _actionType = t; }

void Scene::insertCircuit(QGraphicsSceneMouseEvent *e, CircuitType t) {
    // ako je broj ulaza != -1 ==> korisnik je izabrao broj ulaza
    if (_numberOfInput != -1) {
	_elem = circuit::getElementByType(t, _numberOfInput);
	_numberOfInput = -1;
    } else {
	_elem = circuit::getElementByType(t);
    }
    addItem(_elem);
    _elem->setPos(e->scenePos());
}

auto Scene::getConnectionPoint(Circuit *circuit,
				  QPointF mousePosition) const -> QPointF {
    // Rastojanje klika od tacke
    const int clickRadius = 4;
    QPointF res;
    QPointF objPosition = circuit->scenePos();
    std::vector<QPointF> inPositions = circuit->getInputPositions();
    for (auto &iter : inPositions) {
	int result = std::sqrt(
	    (mousePosition.rx() - (objPosition.rx() + iter.rx())) *
		(mousePosition.rx() - (objPosition.rx() + iter.rx())) +
	    (mousePosition.ry() - (objPosition.ry() + iter.ry())) *
		(mousePosition.ry() - (objPosition.ry() + iter.ry())));

	if (result <= clickRadius) {
	    res.rx() = iter.rx() + objPosition.rx();
	    res.ry() = iter.ry() + objPosition.ry();
	    return res;
	}
    }

    std::vector<QPointF> outPositions = circuit->getOutPutPositions();
    for (auto &iter : outPositions) {
	/*if(iter == nullptr){
	    std::cout <<"NULL" << std::endl;
	}*/
	int result = std::sqrt(
	    (mousePosition.rx() - (objPosition.rx() + iter.rx())) *
		(mousePosition.rx() - (objPosition.rx() + iter.rx())) +
	    (mousePosition.ry() - (objPosition.ry() + iter.ry())) *
		(mousePosition.ry() - (objPosition.ry() + iter.ry())));

	if (result <= clickRadius) {
	    res.rx() = iter.rx() + objPosition.rx();
	    res.ry() = iter.ry() + objPosition.ry();
	    return res;
	}
    }
    return objPosition;
}

void Scene::deleteSelectedItems() {
    auto selected = selectedItems();
    for (auto item : selected) {
	removeItem(item);
	delete item;
    }
}

template <typename Item>
auto compareGraphicsItems(Item frst, Item scnd) -> bool {
    const auto frstPos = frst->scenePos(), scndPos = scnd->scenePos();
    if (frstPos.y() != scndPos.y())
	return frstPos.y() < scndPos.y();
    else
	return frstPos.x() <= scndPos.x();
}

void Scene::exportSceneToFile(const QString &file) const {

    std::vector<CircuitIn *> inputs;
    std::vector<CircuitOut *> outputs;
    std::vector<Circuit *> circuits;
    std::vector<Connection *> connections;

    for (auto item : items()) {
	if (item->isVisible()) {
	    if (dynamic_cast<CircuitIn *>(item)) {
		inputs.push_back(static_cast<CircuitIn *>(item));
		continue;
	    }
	    if (dynamic_cast<CircuitOut *>(item)) {
		outputs.push_back(static_cast<CircuitOut *>(item));
		continue;
	    }
	    if (dynamic_cast<Connection *>(item)) {
		connections.push_back(static_cast<Connection *>(item));
		continue;
	    }
	    if (dynamic_cast<Circuit *>(item)) {
		circuits.push_back(static_cast<Circuit *>(item));
		continue;
	    }
	}
    }
    std::sort(
	inputs.begin(), inputs.end(), [](const auto frst, const auto scnd) {
	    return compareGraphicsItems(frst, scnd);
	});
    std::sort(
	outputs.begin(), outputs.end(), [](const auto frst, const auto scnd) {
	    return compareGraphicsItems(frst, scnd);
	});
    std::sort(
	circuits.begin(), circuits.end(), [](const auto frst, const auto scnd) {
	    return compareGraphicsItems(frst, scnd);
	});

    circuit::scene2file(file, inputs, outputs, circuits, connections);
}

void Scene::setNumberOfInput(int num) { _numberOfInput = num; }

void Scene::setImportFilename(const QString &filename) {
    m_importFileName = filename;
}

void Scene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    QGraphicsScene::mouseReleaseEvent(event);
}

void Scene::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    QGraphicsScene::mouseMoveEvent(event);
}

void Scene::osvezi() {
    clear();
    update();
    resetActions();
}

void Scene::rotateItems(RotationType angle) {
    auto selected = selectedItems();

    if (selected.isEmpty())
	return;
    auto elem = selected.last();
    if (dynamic_cast<Circuit *>(elem)) {
	auto *c = static_cast<Circuit *>(elem);
	_buttonIsSelected = false;
	c->setRotation(angle);
	c->setRotationType(angle);
    }
}
