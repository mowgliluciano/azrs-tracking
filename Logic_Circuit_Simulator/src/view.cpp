#include <QApplication>
#include <QMouseEvent>
#include <QScrollBar>
#include <qmath.h>
#include <view.hpp>

#define MIN_ZOOM 1
#define MAX_ZOOM 30

Graphics_view_zoom::Graphics_view_zoom(QGraphicsView *view)
    : QObject(view)
    , m_view(view)
    , m_zoom_factor_base(1.0015)
    , m_curr_zoom(1)
    , m_min_zoom(MIN_ZOOM)
    , m_max_zoom(MAX_ZOOM) {
    m_view->viewport()->installEventFilter(this);
    m_view->setMouseTracking(true);
}

void Graphics_view_zoom::gentle_zoom(double factor) {
    factor = calculate_factor(factor);
    if (std::abs(1 - factor) > 0.00005) {
	factor = calculate_factor(factor);
	m_view->scale(factor, factor);
	m_view->centerOn(m_target_scene_pos);
	QPointF delta_viewport_pos =
	    m_target_viewport_pos - QPointF(m_view->viewport()->width() / 2.0,
					    m_view->viewport()->height() / 2.0);
	QPointF viewport_center =
	    m_view->mapFromScene(m_target_scene_pos) - delta_viewport_pos;
	m_view->centerOn(m_view->mapToScene(viewport_center.toPoint()));
	emit zoomed();
    }
}

void Graphics_view_zoom::set_zoom_factor_base(double value) {
    m_zoom_factor_base = value;
}

auto Graphics_view_zoom::eventFilter(QObject *object, QEvent *event) -> bool {
    if (event->type() == QEvent::MouseMove) {
	auto *mouse_event = static_cast<QMouseEvent *>(event);
	QPointF delta = m_target_viewport_pos - mouse_event->pos();
	if (qAbs(delta.x()) > 5 || qAbs(delta.y()) > 5) {
	    m_target_viewport_pos = mouse_event->pos();
	    m_target_scene_pos = m_view->mapToScene(mouse_event->pos());
	}
    } else if (event->type() == QEvent::Wheel) {
	auto *wheel_event = static_cast<QWheelEvent *>(event);
	double angle = wheel_event->angleDelta().y();
	double factor = qPow(m_zoom_factor_base, angle);
	gentle_zoom(factor);
	return true;
    }
    Q_UNUSED(object)
    return false;
}

auto Graphics_view_zoom::calculate_factor(double factor) -> double {
    double temp_factor = m_curr_zoom * factor;
    if ((factor < 1 && temp_factor <= m_min_zoom) ||
	(factor > 1 && temp_factor >= m_max_zoom)) {
	factor = ((factor <= 1) ? m_min_zoom : m_max_zoom) / m_curr_zoom;
    }
    m_curr_zoom *= factor;
    return factor;
}

void Graphics_view_zoom::center_zoom(double factor) {
    factor = calculate_factor(factor);
    if (std::abs(1 - factor) > 0.00005) {
	const QPoint center(m_view->viewport()->width() / 2.0,
			    m_view->viewport()->height() / 2.0);
	m_view->scale(factor, factor);

	m_view->centerOn(m_view->mapToScene(center));
	emit zoomed();
    }
}
