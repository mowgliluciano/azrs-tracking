#include "mainwindow.h"
#include "scene.hpp"
#include "ui_mainwindow.h"
#include <QActionGroup>
#include <QFileDialog>
#include <QGraphicsItem>
#include <QGraphicsView>
#include <QInputDialog>
#include <QList>
#include <QMessageBox>
#include <QPainter>
#include <view.hpp>
#define SCENE_WIDTH 32000
#define SCENE_HEIGHT 32000

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    setupScene();
    setupView();
    setupConnections();
    setupActions();
}

MainWindow::~MainWindow() {
    delete ui;
    delete _gvz;
}

void MainWindow::onClickButton(CircuitType t) {
    // proveri da li za dato kolo moze da se postavi broj ulaza
    setInputProperties(t);
    _scene->setCircuitType(t);
}

void MainWindow::setupConnections() {
    connect(ui->AndButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::And);
    });
    connect(ui->OrButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Or);
    });
    connect(ui->IdButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Id);
    });
    connect(ui->InButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::In);
    });
    connect(ui->OutButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Out);
    });
    connect(ui->ClockButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Clock);
    });
    connect(ui->NandButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Nand);
    });
    connect(ui->NorButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Nor);
    });
    connect(ui->XorButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Xor);
    });
    connect(ui->MuxButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Multiplex);
    });
    connect(ui->DemuxButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Demultiplex);
    });
    connect(ui->CoderButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Encoder);
    });
    connect(ui->DecoderButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Decoder);
    });
    connect(ui->srButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::SRFlipFlop);
    });
    connect(ui->jkButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::JKFlipFlop);
    });
    connect(ui->tButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::TFlipFlop);
    });
    connect(ui->dButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::DFlipFlop);
    });
    connect(ui->plusButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Adder);
    });
    connect(ui->minusButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Subtractor);
    });
    connect(ui->NonButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Non);
    });
    connect(ui->SpliterButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Spliter);
    });
    connect(ui->NXorButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Nxor);
    });
    connect(ui->HalfAdderButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::HalfAdder);
    });
    connect(ui->halfSubButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::HalfSubtractor);
    });
    connect(ui->comparatoButt, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::Comparator);
    });
    connect(ui->srLatchButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::SRLatch);
    });
    connect(ui->dLatchButton, &QToolButton::clicked, this, [&]() {
	onClickButton(CircuitType::DLatch);
    });
}

void MainWindow::setupActions() {
    _toolBarActions = new QActionGroup(this);
    _toolBarActions->addAction(ui->actionMove);
    _toolBarActions->addAction(ui->actionDelete);
    _toolBarActions->addAction(ui->actionConnect);
    _toolBarActions->addAction(ui->actionInput);

    _toolBarActions->setExclusive(true);
    ui->actionInput->setEnabled(false);

    connect(_scene, &Scene::resetActions, this, &MainWindow::resetActions);
    connect(
	ui->actionMove, &QAction::triggered, this, &MainWindow::onActionMove);
    connect(ui->actionDelete,
	    &QAction::triggered,
	    this,
	    &MainWindow::onActionDelete);
    connect(ui->actionConnect,
	    &QAction::triggered,
	    this,
	    &MainWindow::onActionConnect);
    connect(ui->actionImport,
	    &QAction::triggered,
	    this,
	    &MainWindow::onActionImport);
    connect(ui->actionExport,
	    &QAction::triggered,
	    this,
	    &MainWindow::onActionExport);
    connect(
	ui->actionInput, &QAction::triggered, this, &MainWindow::onActionInput);
    connect(ui->actionZoom_in,
	    &QAction::triggered,
	    this,
	    &MainWindow::onActionZoomIn);
    connect(ui->actionZoom_out,
	    &QAction::triggered,
	    this,
	    &MainWindow::onActionZoomOut);
    connect(ui->actionRotateLeft,
	    &QAction::triggered,
	    this,
	    &MainWindow::onActionRotateLeft);
    connect(ui->actionRotate_Right,
	    &QAction::triggered,
	    this,
	    &MainWindow::onActionRotateRight);
    connect(
	ui->actionQuit, &QAction::triggered, this, &MainWindow::onActionQuit);
    connect(ui->actionNew, &QAction::triggered, this, &MainWindow::onActionNew);
}

void MainWindow::setupView() {
    ui->graphicsView->setScene(_scene);
    ui->graphicsView->setRenderHint(QPainter::Antialiasing);
    ui->graphicsView->setViewportUpdateMode(
	QGraphicsView::BoundingRectViewportUpdate);
    ui->graphicsView->setBackgroundBrush(Qt::Dense7Pattern);
    ui->graphicsView->setHorizontalScrollBarPolicy(
	Qt::ScrollBarPolicy::ScrollBarAsNeeded);
    ui->graphicsView->setVerticalScrollBarPolicy(
	Qt::ScrollBarPolicy::ScrollBarAsNeeded);
    _gvz = new Graphics_view_zoom(ui->graphicsView);
}

void MainWindow::setupScene() {
    _scene = new Scene(this);
    _scene->setSceneRect(
	QRectF(-SCENE_WIDTH / 2, -SCENE_HEIGHT / 2, SCENE_WIDTH, SCENE_HEIGHT));
    //    _scene->setForegroundBrush(Qt::Dense7Pattern);
}

void MainWindow::setInputProperties(CircuitType t) {
    switch (t) {
    case CircuitType::And:
    case CircuitType::Nand:
    case CircuitType::Or:
    case CircuitType::Nor:
    case CircuitType::Xor:
    case CircuitType::Nxor:
	_inputType = InputType::Gates;
	ui->actionInput->setEnabled(true);
	break;
    case CircuitType::Multiplex:
    case CircuitType::Demultiplex:
    case CircuitType::Encoder:
    case CircuitType::Decoder:
	_inputType = InputType::Plexers;
	ui->actionInput->setEnabled(true);
	break;
    default:
	// ostalima ne moze da se promeni broj ulaza
	ui->actionInput->setEnabled(false);
	break;
    }
}

void MainWindow::onActionImport() {
    auto fileName = QFileDialog::getOpenFileName(
	this, tr("Open Circuit"), ".", tr("Circuit Files (*.circuit)"));

    if (!fileName.isEmpty()) {
	_scene->setActionType(ActionType::Import);
	_scene->setImportFilename(fileName);
    }
}

void MainWindow::onActionExport() {
    QString fileName = QFileDialog::getSaveFileName(
	this, tr("Save Circuit"), ".", tr("Circuit (*.circuit)"));
    if (!fileName.isEmpty()) {
	_scene->exportSceneToFile(fileName);
    }
}

void MainWindow::onActionMove() { _scene->setActionType(ActionType::Move); }

void MainWindow::onActionDelete() {
    _scene->deleteSelectedItems();
    ui->actionDelete->setChecked(false);
}

void MainWindow::onActionConnect() {
    _scene->setActionType(ActionType::Connect);
}

void MainWindow::resetActions() {
    auto currChecked = _toolBarActions->checkedAction();
    if (currChecked != nullptr)
	currChecked->setChecked(false);
}

void MainWindow::onActionInput() {
    bool ok;
    unsigned numberOfInput;
    if (_inputType == InputType::Gates)
	numberOfInput = QInputDialog::getInt(
	    this, "Gate", "Enter number of input:", 2, 2, 4, 1, &ok);
    //        std::cout << numberOfInput << std::endl;
    else
	numberOfInput = QInputDialog::getInt(
	    this, "Plexer", "Enter number of input:", 2, 2, 3, 1, &ok);

    _scene->setNumberOfInput(numberOfInput);
}

void MainWindow::onActionZoomIn() { _gvz->center_zoom(1.5); }

void MainWindow::onActionZoomOut() { _gvz->center_zoom(0.70); }

// void MainWindow::onActionMinimize()
//{
//     MainWindow::showMinimized();
//}

// void MainWindow::onActionMaximize()
//{
//     MainWindow::showMaximized();
//}

void MainWindow::onActionQuit() {

    QMessageBox::StandardButton response = QMessageBox::question(
	this,
	"Quit",
	"Force it to quit?",
	QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
	QMessageBox::Yes);

    if (response == QMessageBox::Yes)
	MainWindow::close();
    else
	return;
}

void MainWindow::onActionNew() {
    setWindowTitle(QString("Circuit Designer"));
    _scene->osvezi();
}

void MainWindow::onActionRotateLeft() { ui->graphicsView->rotate(-8); }

void MainWindow::onActionRotateRight() { ui->graphicsView->rotate(8); }
