#include <algorithm>
#include <catch.hpp>
#include <circuitutil.hpp>
#include <iostream>
#include <numeric>
#include <util.hpp>

using namespace std;

//***************************************************************************************
TEST_CASE("Izracunavanje izlaz1099a Clock kola za razlicite ulaze", "[clock]") {
    SECTION("Kada se instancira ClockCircuit, ocekujem da je broj ulaza 0") {
	CircuitClock clk{};
	const auto excepted = 0;

	const auto result = clk.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se instancira ClockCircuit, ocekujem da je broj izlaza 1") {
	CircuitClock clk{};
	const auto excepted = 1;

	const auto result = clk.getOutputN();

	REQUIRE(result == excepted);
    }
    SECTION("Kada se instancira ClockCircuit, pocetna vrednost je "
	    "SignalType::False") {
	CircuitClock clk{};
	const auto excepted = SignalType::False;

	const auto result = clk.getSignal().lock()->value();
	REQUIRE(result == excepted);
    }
    SECTION("Kada se instancira ClockCircuit i nakon 1 evaluacije, tada je "
	    "vrednost na izlazu SignalType::True") {
	CircuitClock clk{};
	const auto excepted = SignalType::True;

	clk.evaluate();
	const auto result = clk.getSignal().lock()->value();

	REQUIRE(result == excepted);
    }
    SECTION("Kada se instancira ClockCircuit i nakon 2 evaluacije, tada je "
	    "vrednost na izlazu SignalType::False") {
	CircuitClock clk{};
	const auto excepted = SignalType::False;

	clk.evaluate();
	clk.evaluate();

	const auto result = clk.getSignal().lock()->value();

	REQUIRE(result == excepted);
    }
}

//***************************************************************************************
TEST_CASE("Izrazcunavanje izlaza Spliter kola za razlicite ulaze ",
	  "[spliter]") {
    SECTION("Kada se instancira CircuitSpliter , ocekujem da je broj ulaza 1") {
	CircuitSpliter clk{};
	const auto excepted = 1;

	const auto result = clk.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se instancira CircuitSpliter podrazumevanim konstruktorom, "
	    "ocekujem da je broj izlaza 3") {
	CircuitSpliter s{};
	const auto excepted = 3;

	const auto result = s.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION(
	"Kada se instancira CircuitSpliter sa vrednoscu, ocekujem da je broj "
	"izlaza ta vrednost") {
	const unsigned excepted = 3;
	CircuitSpliter s{excepted};

	const auto result = s.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada na ulazu CircuitSpliter niko nije povezan, tada su sve "
	    "vrednosti na izlazu SignatlType::NotConnected") {

	const auto excepted = 5;
	CircuitSpliter s{excepted};

	const auto all_outputs = s.getAllSignals();
	const auto result = std::count(
	    cbegin(all_outputs), cend(all_outputs), SignalType::Not_Connected);
	//  const auto result = all_outputs.size();
	// std::cout << result << std::endl;
	REQUIRE(result == excepted);
    }

    SECTION(
	"Kada je  ulaz CircuitSpliter povezan SignalType::True ili "
	"SignalType::False,tada su sve vrednosti na izlazu SignatlType::True "
	"odnosno SignalType::False") {
	CircuitSpliter s1{5};
	auto sig1 = std::make_shared<Signal>(SignalType::True);
	s1.connectInput(sig1);
	auto sig2 = std::make_shared<Signal>(SignalType::False);
	CircuitSpliter s2{};
	s2.connectInput(sig2);
	s1.evaluate();
	s2.evaluate();
	const auto excepted1 = 5;
	const auto excepted2 = 3;

	const auto all_outputs1 = s1.getAllSignals();
	const auto all_outputs2 = s2.getAllSignals();
	const auto result1 = std::count(
	    cbegin(all_outputs1), cend(all_outputs1), SignalType::True);
	const auto result2 = std::count(
	    cbegin(all_outputs2), cend(all_outputs2), SignalType::False);

	CHECK(result1 == excepted1);
	CHECK(result2 == excepted2);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza CircuitAnd za razlicite ulaze", "[and]") {

    SECTION(
	"Kada se CircuitAnd instancira sa podrazumevanim konstruktorom, broj "
	"ulaza je 2") {
	CircuitAnd a{};
	const auto excepted = 2u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION(
	"Kada se CircuitAnd instancira sa podrazumevanim konstruktorom, broj "
	"izlaza 1") {
	CircuitAnd a{};
	const auto excepted = 1u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se CircuitAnd instancira sa number => broj izlaza je 1") {
	const auto input = 10u;
	CircuitAnd a{input};
	const auto excepted = 1u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada bar 1 ulaz CircuitAnd nije povezan => na izlazu je "
	    "SignalType::NotConnected") {
	CircuitAnd a{};
	auto sig1 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig1, 0);
	a.evaluate();
	const auto excepted = SignalType::Not_Connected;

	const auto result = a.getSignal().lock()->value();

	REQUIRE(result == excepted);
    }

    SECTION("Kada je svaki ulaz CircuitAnd povezan  => na izlazu je vrednost "
	    "konjukcije ulaza") {
	CircuitAnd a{2};
	auto sig1 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig1, 0);
	auto sig2 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig2, 1);
	a.evaluate();
	const auto excepted = SignalType::False;

	const auto result = a.getSignal().lock()->value();
	;

	REQUIRE(result == excepted);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza CircuitOr za razlicite ulaze", "[or]") {

    SECTION(
	"Kada se CircuitOr instancira sa podrazumevanim konstruktorom, broj "
	"ulaza je 2") {
	CircuitOr a{};
	const auto excepted = 2u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION(
	"Kada se CircuitOr instancira sa podrazumevanim konstruktorom, broj "
	"izlaza 1") {
	CircuitOr a{};
	const auto excepted = 1u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se CircuitOr instancira sa number => broj izlaza je 1") {
	const auto input = 6u;
	CircuitOr a{input};
	const auto excepted = 1u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada bar 1 ulaz CircuitOr nije povezan => na izlazu je "
	    "SignalType::NotConnected") {
	CircuitOr a{};
	auto sig1 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig1, 0);
	a.evaluate();
	const auto excepted = SignalType::Not_Connected;

	const auto result = a.getSignal().lock()->value();
	;

	REQUIRE(result == excepted);
    }

    SECTION("Kada je svaki ulaz CircuitOr povezan  => na izlazu je vrednost "
	    "disjunkcije ulaza") {
	CircuitOr a{2};
	auto sig1 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig1, 0);
	auto sig2 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig2, 1);
	a.evaluate();
	const auto excepted = SignalType::True;

	const auto result = a.getSignal().lock()->value();
	;

	REQUIRE(result == excepted);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza CircuitNor za razlicite ulaze", "[nor]") {

    SECTION(
	"Kada se CircuitOr instancira sa podrazumevanim konstruktorom, broj "
	"ulaza je 2") {
	CircuitNor a{};
	const auto excepted = 2u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION(
	"Kada se CircuitNor instancira sa podrazumevanim konstruktorom, broj "
	"izlaza 1") {
	CircuitNor a{};
	const auto excepted = 1u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se CircuitNor instancira sa number => broj izlaza je 1") {
	const auto input = 6u;
	CircuitNor a{input};
	const auto excepted = 1u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada bar 1 ulaz CircuitNor nije povezan => na izlazu je "
	    "SignalType::NotConnected") {
	CircuitNor a{};
	auto sig1 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig1, 0);
	a.evaluate();
	const auto excepted = SignalType::Not_Connected;

	const auto result = a.getSignal().lock()->value();
	;

	REQUIRE(result == excepted);
    }

    SECTION("Kada je svaki ulaz CircuitNor povezan  => na izlazu je vrednost "
	    "negacija disjunkcije ulaza") {
	CircuitNor a{2};
	auto sig1 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig1, 0);
	auto sig2 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig2, 1);
	a.evaluate();
	const auto excepted = SignalType::False;

	const auto result = a.getSignal().lock()->value();
	;

	REQUIRE(result == excepted);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza CircuitNand za razlicite ulaze", "[nand]") {

    SECTION("Kada se CircuitNand instancira sa podrazumevanim konstruktorom, "
	    "broj ulaza je 2") {
	CircuitNand a{};
	const auto excepted = 2u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se CircuitNand instancira sa podrazumevanim konstruktorom, "
	    "broj izlaza 1") {
	CircuitNand a{};
	const auto excepted = 1u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se CircuitNand instancira sa number => broj izlaza je 1") {
	const auto input = 8u;
	CircuitNand a{input};
	const auto excepted = 1u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada bar 1 ulaz CircuitNand nije povezan => na izlazu je "
	    "SignalType::NotConnected") {
	CircuitNand a{};
	auto sig1 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig1, 0);
	a.evaluate();
	const auto excepted = SignalType::Not_Connected;

	const auto result = a.getSignal().lock()->value();
	;

	REQUIRE(result == excepted);
    }

    SECTION("Kada je svaki ulaz CircuitNand povezan  => na izlazu je vrednost "
	    "negacija disjunkcije ulaza") {
	CircuitNand a{3};
	auto sig1 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig1, 0);
	auto sig2 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig2, 1);
	auto sig3 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig3, 2);
	a.evaluate();
	const auto excepted = SignalType::True;

	const auto result = a.getSignal().lock()->value();
	;

	REQUIRE(result == excepted);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza CircuitXor za razlicite ulaze", "[xor]") {

    SECTION(
	"Kada se CircuitXor instancira sa podrazumevanim konstruktorom, broj "
	"ulaza je 2") {
	CircuitXor a{};
	const auto excepted = 2u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION(
	"Kada se CircuitXor instancira sa podrazumevanim konstruktorom, broj "
	"izlaza 1") {
	CircuitXor a{};
	const auto excepted = 1u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se CircuitXor instancira sa number => broj izlaza je 1") {
	const auto input = 6u;
	CircuitXor a{input};
	const auto excepted = 1u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada bar 1 ulaz CircuitXor nije povezan => na izlazu je "
	    "SignalType::NotConnected") {
	CircuitXor a{};
	auto sig1 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig1, 0);
	a.evaluate();
	const auto excepted = SignalType::Not_Connected;

	const auto result = a.getSignal().lock()->value();
	;

	REQUIRE(result == excepted);
    }

    SECTION("Kada je svaki ulaz CircuitXor povezan  => na izlazu je vrednost "
	    "ekskluzivne disjunkcije ulaza") {
	CircuitXor a{2};
	auto sig1 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig1, 0);
	auto sig2 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig2, 1);
	a.evaluate();
	const auto excepted = SignalType::False;

	const auto result = a.getSignal().lock()->value();

	REQUIRE(result == excepted);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza CircuitNxor za razlicite ulaze", "[nxor]") {

    SECTION("Kada se CircuitNxor instancira sa podrazumevanim konstruktorom, "
	    "broj ulaza je 2") {
	CircuitNxor a{};
	const auto excepted = 2u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se CircuitNxor instancira sa podrazumevanim konstruktorom, "
	    "broj izlaza 1") {
	CircuitNxor a{};
	const auto excepted = 1u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se CircuitNxor instancira sa number => broj izlaza je 1") {
	const auto input = 6u;
	CircuitNxor a{input};
	const auto excepted = 1u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada bar 1 ulaz CircuitNxor nije povezan => na izlazu je "
	    "SignalType::NotConnected") {
	CircuitNxor a{};
	auto sig1 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig1, 0);
	a.evaluate();
	const auto excepted = SignalType::Not_Connected;

	const auto result = a.getSignal().lock()->value();
	;

	REQUIRE(result == excepted);
    }

    SECTION("Kada je svaki ulaz CircuitNxor povezan  => na izlazu je vrednost "
	    "negacija ekskluzivne disjunkcije ulaza") {
	CircuitNxor a{2};
	auto sig1 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig1, 0);
	auto sig2 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig2, 1);
	a.evaluate();
	const auto excepted = SignalType::True;

	const auto result = a.getSignal().lock()->value();
	;

	REQUIRE(result == excepted);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza CircuitNon za razlicite ulaze", "[non]") {

    SECTION(
	"Kada se CircuitNon instancira sa podrazumevanim konstruktorom, broj "
	"ulaza je 1") {
	CircuitNon a{};
	const auto excepted = 1u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION(
	"Kada se CircuitNon instancira sa podrazumecanim konstruktorom, broj "
	"izlaza je 1") {
	CircuitNon a{};
	const auto excepted = 1u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION(
	"Kada je ulaz CircuitNon povezan  => na izlazu je vrednost negacije "
	"ulaza") {
	CircuitNon a{};
	auto sig = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig, 0);
	a.evaluate();
	const auto excepted = SignalType::False;

	const auto result = a.getSignal().lock()->value();
	;

	REQUIRE(result == excepted);
    }

    SECTION("Kada ulaz CircuitNon nije povezan  => na izlazu je vrednost "
	    "SignalType::NotConnected") {
	CircuitNon a{};
	auto sig = std::make_shared<Signal>(SignalType::Not_Connected);
	a.connectInput(sig, 0);
	a.evaluate();
	const auto excepted = SignalType::Not_Connected;

	const auto result = a.getSignal().lock()->value();
	;

	REQUIRE(result == excepted);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza Multiplexera za razlicite ulaze",
	  "[multiplexer]") {

    SECTION("Kada se Multiplexer instancira sa podrazumevanim konstruktorom, "
	    "broj ulaza je 6") {
	Multiplexer a(4, 2);
	const auto excepted = 6u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION(
	"Kada su oba kontrolna ulaza True, prosledjuje se vrednost 4. ulaza") {
	Multiplexer a(4, 2);

	auto sig1 = std::make_shared<Signal>(SignalType::True);
	auto sig2 = std::make_shared<Signal>(SignalType::True);
	auto sig3 = std::make_shared<Signal>(SignalType::False);
	auto sig4 = std::make_shared<Signal>(SignalType::True);
	auto control1 = std::make_shared<Signal>(SignalType::True);
	auto control2 = std::make_shared<Signal>(SignalType::True);

	a.connectInput(sig1, 0);
	a.connectInput(sig2, 1);
	a.connectInput(sig3, 2);
	a.connectInput(sig4, 3);
	a.connectInput(control1, 4);
	a.connectInput(control2, 5);
	a.evaluate();

	const auto expected = SignalType::True;

	const auto result = a.getSignal().lock()->value();
	REQUIRE(result == expected);
    }

    //    SECTION("Ulazi koji nisu konektovani ne bi trebalo da uticu na izlaz")
    //    {
    //        Multiplexer a(4,2);

    //        auto sig1 = std::make_shared<Signal>(SignalType::True);
    //        auto sig4 = std::make_shared<Signal>(SignalType::True);
    //        auto control1 = std::make_shared<Signal>(SignalType::True);
    //        auto control2 = std::make_shared<Signal>(SignalType::True);

    //        a.connectInput(sig1, 0);
    //        a.connectInput(sig4, 3);
    //        a.connectInput(control1, 4);
    //        a.connectInput(control2, 5);
    //        a.evaluate();

    //        const auto expected = SignalType::True;

    //        const auto result = a.getSignal().lock()->value();;
    //        REQUIRE(result == expected);
    //    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza Demultiplexera za razlicite ulaze",
	  "[demultiplexer]") {

    SECTION("Kada se Demultiplexer instancira sa podrazumevanim konstruktorom, "
	    "broj ulaza je 3") {
	Demultiplexer a(1, 2);
	const auto excepted = 3u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION("Oba kontrolna ulaza False, Signal se salje na prvi izlaz") {
	Demultiplexer a(1, 2);

	auto input = std::make_shared<Signal>(SignalType::True);
	auto control1 = std::make_shared<Signal>(SignalType::False);
	auto control2 = std::make_shared<Signal>(SignalType::False);

	a.connectInput(input, 0);
	a.connectInput(control1, 1);
	a.connectInput(control2, 2);
	a.evaluate();

	const auto result = a.getSignal(0).lock()->value();

	const auto expected = SignalType::True;
	REQUIRE(result == expected);
    }

    SECTION("Ako nije konektovan ulaz salje not connected na sve izlaze") {
	Demultiplexer a(1, 2);

	a.evaluate();

	const auto result = a.getSignal(0).lock()->value();

	const auto expected = SignalType::Not_Connected;
	REQUIRE(result == expected);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza Encodera za razlicite ulaze", "[encoder]") {

    SECTION("Kada se Encoder instancira sa podrazumevanim konstruktorom, broj "
	    "ulaza je 4") {
	CircuitEncoder a(4, 2);
	const auto excepted = 4u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza CircuitComparator za razlicite ulaze",
	  "[comparator]") {

    SECTION("Kada se CircuitComparator instancira sa podrazumevanim "
	    "konstruktorom, broj ulaza je 2") {
	CircuitComparator a{};
	const auto excepted = 2u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se CircuitComparator instancira sa podrazumevanim "
	    "konstruktorom, broj izlaza je 3") {
	CircuitComparator a{};
	const auto excepted = 3u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada bar 1 ulaz CircuitComparator nije povezan => na izlazu je "
	    "SignalType::NotConnected") {

	CircuitComparator a{};
	a.evaluate();

	const auto result = a.getSignal(0).lock()->value();

	const auto expected = SignalType::Not_Connected;
	REQUIRE(result == expected);
    }

    SECTION("Kada je svaki ulaz CircuitComparator povezan  => na izlazu je "
	    "vrednost dobijena odgovarajucim poredjenjem ulaza") {
	CircuitComparator a{};
	auto sig1 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig1, 0);
	auto sig2 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig2, 1);
	a.evaluate();
	const auto excepted1 = SignalType::False;
	const auto excepted2 = SignalType::True;
	const auto excepted3 = SignalType::False;

	const auto result1 = a.getSignal(0).lock()->value();
	const auto result2 = a.getSignal(1).lock()->value();
	const auto result3 = a.getSignal(2).lock()->value();

	REQUIRE(result1 == excepted1);
	REQUIRE(result2 == excepted2);
	REQUIRE(result3 == excepted3);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza In kola za razlicite ulaze", "[in]") {
    SECTION("Kada se instancira CircuitIn, ocekujem da je broj ulaza 0") {
	CircuitIn in{};
	const auto excepted = 0;

	const auto result = in.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se instancira CircuitIn, ocekujem da je broj izlaza 1") {
	CircuitIn in{};
	const auto excepted = 1;

	const auto result = in.getOutputN();

	REQUIRE(result == excepted);
    }
    SECTION(
	"Kada se instancira CircuitIn, pocetna vrednost je SignalType::True") {
	CircuitIn in{};
	const auto excepted = SignalType::True;

	const auto result = in.getSignal().lock()->value();
	REQUIRE(result == excepted);
    }

    SECTION("Kada se instancira CircuitIn sa argumentom true, tada je vrednost "
	    "na izlazu SignalType::True") {
	CircuitIn in{true};
	const auto excepted = SignalType::True;

	const auto result = in.getSignal().lock()->value();

	REQUIRE(result == excepted);
    }

    SECTION(
	"Kada se instancira CircuitIn sa argumentom false, tada je vrednost "
	"na izlazu SignalType::False") {
	CircuitIn in{false};
	const auto excepted = SignalType::False;

	const auto result = in.getSignal().lock()->value();

	REQUIRE(result == excepted);
    }

    SECTION(
	"Kada se instancira CircuitIn i nakon 1 evaluacije, tada je vrednost "
	"na izlazu SignalType::False") {
	CircuitIn in{};
	const auto excepted = SignalType::False;

	in.evaluate();

	const auto result = in.getSignal().lock()->value();

	REQUIRE(result == excepted);
    }
    SECTION(
	"Kada se instancira CircuitIn i nakon 2 evaluacije, tada je vrednost "
	"na izlazu SignalType::True") {
	CircuitIn in{};
	const auto excepted = SignalType::True;

	in.evaluate();
	in.evaluate();

	const auto result = in.getSignal().lock()->value();

	REQUIRE(result == excepted);
    }
}

//***************************************************************************************
TEST_CASE("Testiranje propagacije signala kroz kola", "[connect]") {
    SECTION("Slanje signala iz CircuitIn na CircuitOut pre i posle promene "
	    "vrednosti Ulaznog kola") {
	const auto expected1 = SignalType::True, expected2 = SignalType::False;

	CircuitIn in{};
	CircuitOut out{};

	circuit::connect(in, 0, out, 0);
	const auto result1 = out.value();
	in.evaluate();
	const auto result2 = out.value();

	REQUIRE(result1 == expected1);
	REQUIRE(result2 == expected2);
    }

    SECTION("Slanje signala iz CircuitIn kroz kolo negacije na CircuitOut") {
	const auto expected = SignalType::False;

	CircuitIn in{};
	CircuitNon non{};
	CircuitOut out{};

	circuit::connect(in, 0, non, 0);
	circuit::connect(non, 0, out, 0);
	const auto result = out.value();

	REQUIRE(result == expected);
    }

    SECTION("Provera propagacije signala pri otkacivanju kola") {
	const auto expected = SignalType::Not_Connected;

	CircuitIn in{};
	CircuitOut out{};

	circuit::connect(in, 0, out, 0);
	in.evaluate();
	const auto result1 = out.value();

	circuit::disconnect(out, 0);
	const auto result2 = out.value();

	REQUIRE(result1 != expected);
	REQUIRE(result2 == expected);
    }

    SECTION("Provera propagacije signala pri brisanju kola") {
	const auto expected = SignalType::Not_Connected;

	Circuit *in = circuit::getElementByType(CircuitType::In);
	CircuitOut out{};

	circuit::connect(in, 0, &out, 0);
	in->evaluate();
	const auto result1 = out.value();

	delete in;
	const auto result2 = out.value();

	REQUIRE(result1 != expected);
	REQUIRE(result2 == expected);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza CircuitHalfAdder za razlicite ulaze",
	  "[halfadder]") {

    SECTION("Kada se CircuitHalfAdder instancira sa podrazumevanim "
	    "konstruktorom, broj ulaza je 2") {
	CircuitHalfAdder a{};
	const auto excepted = 2u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se CircuitHalfAdder instancira sa podrazumevanim "
	    "konstruktorom, broj izlaza je 2") {
	CircuitHalfAdder a{};
	const auto excepted = 2u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada bar 1 ulaz CircuitHalfAdder nije povezan => na izlazu je "
	    "SignalType::NotConnected") {

	CircuitHalfAdder a{};
	a.evaluate();

	const auto result = a.getSignal(0).lock()->value();

	const auto expected = SignalType::Not_Connected;
	REQUIRE(result == expected);
    }

    SECTION("Kada je svaki ulaz CircuitHalfAdder povezan  => na izlazu su dve "
	    "vrednosti- suma i ostatak") {
	CircuitHalfAdder a{};
	auto sig1 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig1, 0);
	auto sig2 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig2, 1);
	a.evaluate();
	const auto excepted1 = SignalType::False;
	const auto excepted2 = SignalType::True;

	const auto result1 = a.getSignal(0).lock()->value();
	const auto result2 = a.getSignal(1).lock()->value();

	REQUIRE(result1 == excepted1);
	REQUIRE(result2 == excepted2);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza CircuitAdder za razlicite ulaze", "[adder]") {

    SECTION("Kada se CircuitAdder instancira sa podrazumevanim konstruktorom, "
	    "broj ulaza je 3") {
	CircuitAdder a{};
	const auto excepted = 3u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se CircuitAdder instancira sa podrazumevanim konstruktorom, "
	    "broj izlaza je 2") {
	CircuitAdder a{};
	const auto excepted = 2u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada bar 1 ulaz CircuitAdder nije povezan => na izlazu je "
	    "SignalType::NotConnected") {

	CircuitAdder a{};
	a.evaluate();

	const auto result = a.getSignal(0).lock()->value();

	const auto expected = SignalType::Not_Connected;
	REQUIRE(result == expected);
    }

    SECTION("Kada je svaki ulaz CircuitAdder povezan  => na izlazu su dve "
	    "vrednosti- suma i ostatak") {
	CircuitAdder a{};
	auto sig1 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig1, 0);
	auto sig2 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig2, 1);
	auto sig3 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig3, 2);
	a.evaluate();
	const auto excepted1 = SignalType::False;
	const auto excepted2 = SignalType::True;

	const auto result1 = a.getSignal(0).lock()->value();
	const auto result2 = a.getSignal(1).lock()->value();

	REQUIRE(result1 == excepted1);
	REQUIRE(result2 == excepted2);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza CircuitHalfSubtractor za razlicite ulaze",
	  "[halfsubtracor]") {

    SECTION("Kada se CircuitHalfSubtractor instancira sa podrazumevanim "
	    "konstruktorom, broj ulaza je 2") {
	CircuitHalfSubtractor a{};
	const auto excepted = 2u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se CircuitHalfSubtractor instancira sa podrazumevanim "
	    "konstruktorom, broj izlaza je 2") {
	CircuitHalfSubtractor a{};
	const auto excepted = 2u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION(
	"Kada bar 1 ulaz CircuitHalfSubtractor nije povezan => na izlazu je "
	"SignalType::NotConnected") {

	CircuitHalfSubtractor a{};
	a.evaluate();

	const auto result = a.getSignal(0).lock()->value();

	const auto expected = SignalType::Not_Connected;
	REQUIRE(result == expected);
    }

    SECTION("Kada je svaki ulaz CircuitHalfSubtractor povezan  => na izlazu su "
	    "dve vrednosti- razlika i prenos") {
	CircuitHalfSubtractor a{};
	auto sig1 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig1, 0);
	auto sig2 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig2, 1);
	a.evaluate();
	const auto excepted1 = SignalType::False;
	const auto excepted2 = SignalType::False;

	const auto result1 = a.getSignal(0).lock()->value();
	const auto result2 = a.getSignal(1).lock()->value();

	REQUIRE(result1 == excepted1);
	REQUIRE(result2 == excepted2);
    }
}

//***************************************************************************************
TEST_CASE("Izracunavanje izlaza CircuitSubtractor za razlicite ulaze",
	  "[subtractor]") {

    SECTION("Kada se CircuitSubtractor instancira sa podrazumevanim "
	    "konstruktorom, broj ulaza je 3") {
	CircuitSubtractor a{};
	const auto excepted = 3u;

	const auto result = a.getInputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada se CircuitSubtractor instancira sa podrazumevanim "
	    "konstruktorom, broj izlaza je 2") {
	CircuitSubtractor a{};
	const auto excepted = 2u;

	const auto result = a.getOutputN();

	REQUIRE(result == excepted);
    }

    SECTION("Kada bar 1 ulaz CircuitSubtractor nije povezan => na izlazu je "
	    "SignalType::NotConnected") {

	CircuitSubtractor a{};
	a.evaluate();

	const auto result = a.getSignal(0).lock()->value();

	const auto expected = SignalType::Not_Connected;
	REQUIRE(result == expected);
    }

    SECTION("Kada je svaki ulaz CircuitSubtractor povezan  => na izlazu su dve "
	    "vrednosti- razlika i prenos") {
	CircuitSubtractor a{};
	auto sig1 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig1, 0);
	auto sig2 = std::make_shared<Signal>(SignalType::True);
	a.connectInput(sig2, 1);
	auto sig3 = std::make_shared<Signal>(SignalType::False);
	a.connectInput(sig3, 2);
	a.evaluate();
	const auto excepted1 = SignalType::False;
	const auto excepted2 = SignalType::False;

	const auto result1 = a.getSignal(0).lock()->value();
	const auto result2 = a.getSignal(1).lock()->value();

	REQUIRE(result1 == excepted1);
	REQUIRE(result2 == excepted2);
    }
}

//***************************************************************************************
TEST_CASE("Provera ispravnosti citanja i pisanja kola", "[parsing]") {
    SECTION("Kada se parsiraju prazni vektori, dobije se prazno kolo") {
	std::vector<CircuitIn *> in{};
	std::vector<CircuitOut *> out{};
	std::vector<Circuit *> inner{};
	std::vector<Connection *> conn{};
	QString file("testiranje.circuit");

	const unsigned expectedin(0), expectedout(0), expectedinner(0);

	circuit::scene2file(file, in, out, inner, conn);
	auto mc = circuit::file2mc(file);
	const auto resultin = mc->getInputN();
	const auto resultout = mc->getOutputN();
	const auto resultinner = mc->getInnerN();

	REQUIRE(mc != nullptr);
	REQUIRE(resultin == expectedin);
	REQUIRE(resultout == expectedout);
	REQUIRE(resultinner == expectedinner);

	delete mc;
    }

    SECTION("Parsiraju se vektori koji imaju elemente, ali bez konekcija.") {
	std::vector<CircuitIn *> in{new CircuitIn(), new CircuitIn()};
	std::vector<CircuitOut *> out{new CircuitOut()};
	std::vector<Circuit *> inner{new CircuitSpliter(),
				     new CircuitAdder(),
				     new CircuitNand(),
				     new CircuitId(),
				     new JKFlipFlop()};
	std::vector<Connection *> conn{};
	QString file("testiranje.circuit");

	const unsigned expectedin(in.size()), expectedout(out.size()),
	    expectedinner(inner.size());

	circuit::scene2file(file, in, out, inner, conn);
	auto mc = circuit::file2mc(file);

	REQUIRE(mc != nullptr);

	const auto resultin = mc->getInputN();
	const auto resultout = mc->getOutputN();
	const auto resultinner = mc->getInnerN();

	REQUIRE(resultin == expectedin);
	REQUIRE(resultout == expectedout);
	REQUIRE(resultinner == expectedinner);

	delete mc;
	for (auto c : in)
	    delete c;
	for (auto c : out)
	    delete c;
	for (auto c : inner)
	    delete c;
    }

    SECTION("Parsiraju se vektori koji imaju elemente, sa konekcijama.") {
	QString file("testiranje.circuit");
	std::vector<CircuitIn *> in{new CircuitIn(), new CircuitIn()};
	std::vector<CircuitOut *> out{new CircuitOut(), new CircuitOut()};
	std::vector<Circuit *> inner{new CircuitSpliter(5),
				     new CircuitEncoder(),
				     new CircuitNand(),
				     new CircuitId(),
				     new CircuitNon(),
				     new CircuitNon(),
				     new CircuitClock()};

	std::vector<Connection *> conn{new Connection(in[0], out[0], 0, 0),
				       new Connection(in[1], inner[0], 0, 0),
				       new Connection(inner[0], inner[5], 2, 0),
				       new Connection(inner[0], inner[2], 1, 1),
				       new Connection(inner[5], inner[2], 0, 0),
				       new Connection(inner[2], out[1], 0, 0)};

	const unsigned expectedin(in.size()), expectedout(out.size()),
	    expectedinner(inner.size());

	circuit::scene2file(file, in, out, inner, conn);
	auto mc = circuit::file2mc(file);

	REQUIRE(mc != nullptr);

	const auto resultin = mc->getInputN();
	const auto resultout = mc->getOutputN();
	const auto resultinner = mc->getInnerN();

	REQUIRE(resultin == expectedin);
	REQUIRE(resultout == expectedout);
	REQUIRE(resultinner == expectedinner);

	delete mc;
	for (auto c : in)
	    delete c;
	for (auto c : out)
	    delete c;
	for (auto c : inner)
	    delete c;
    }
}
